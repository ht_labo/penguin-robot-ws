#ifndef MATH_UTIL_H
#define MATH_UTIL_H

inline float SecureInv(float in) {
    return (in == 0.0f) ? 0.0f : 1.0f / in;
}

#endif // MATH_UTIL_H