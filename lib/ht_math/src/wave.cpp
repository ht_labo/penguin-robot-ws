#include "wave.h"
#include "math_constant.h"
#include <math.h>

Wave::Wave() {

}

void Wave::Config(const WaveForm& form) {
  form_ = form;
}

void Wave::Config(unsigned char type, float amplitude, float base, float frequency, float phase) {
  form_.type = type;
  form_.amplitude = amplitude;
  form_.base = base;
  form_.frequency = frequency;
  form_.phase = phase;
}

float Wave::Update(float time) {
  float ret = form_.base;
  const float ratio = fmod(time * form_.frequency + form_.phase / (2 * PI), 1.0);
  switch(form_.type) {
    case WaveType::CONST:
    ret += 0.0f;
    break;
    case WaveType::SIN:
    ret += form_.amplitude * sin(2 * PI * form_.frequency * time + form_.phase);
    break;
    case WaveType::RECTANGLE:
    if (ratio < 0.5f) {
      ret += form_.amplitude;
    } else {
      ret += -form_.amplitude;
    }
    break;
    case WaveType::TRIANGLE:
    if (ratio < 0.5f) {
      ret += 2 * form_.amplitude * ratio;
    } else {
      ret += 2 * form_.amplitude * (ratio - 1);
    }
    break;
    default:
    break;
  }
  return ret;
}