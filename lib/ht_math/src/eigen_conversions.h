#ifndef EIGEN_CONVERSIONS_H
#define EIGEN_CONVERSIONS_H

#include "numeric_typedefs.h"
#include <array>
#include <vector>

/**
 * @brief std::array配列型をEigenVectorに変換する
 */
template <typename T, size_t N>
Eigen::Matrix<T, N, 1> ArrayToEigenVector(const std::array<T, N>& in);

/**
 * @brief std::vector配列型をEigenVectorに変換する
 */
template <typename T, size_t N>
Eigen::Matrix<T, N, 1> VectorToEigenVector(const std::vector<T>& in);

/**
 * @brief VectorXdをstd::arrayに変換する
 */
template <int ROWS, typename T, size_t ARRAY_ROWS>
void EigenVectorToArray(const Eigen::Matrix<T, ROWS, 1>& in, std::array<T, ARRAY_ROWS>& out);

/**
 * @brief VectorXdをstd::vectorに変換する
 */
template <int ROWS, typename T>
void EigenVectorToVector(const Eigen::Matrix<T, ROWS, 1>& in, std::vector<T>& out);

/**
 * @brief Eigen::Matrixをstd::arrayに変換する
 */
template <typename T, int ROWS, int COLS, size_t R, size_t C>
void EigenMatrixToArray(const Eigen::Matrix<T, ROWS, COLS>& in, std::array<std::array<T, R>, C>& out);

template<typename T, size_t N>
Eigen::Matrix<T, N, 1> ArrayToEigenVector(const std::array<T, N>& in) {
  Eigen::Matrix<T, N, 1> out;
  std::copy(in.begin(), in.end(), out.data());
  return out;
}

template<typename T, size_t N>
Eigen::Matrix<T, Eigen::Dynamic, 1> VectorToEigenVector(const std::vector<T>& in) {
  Eigen::Matrix<T, Eigen::Dynamic, 1> out(in.size());
  std::copy(in.begin(), in.end(), out.data());
  return out;
}

template<int ROWS, typename T, size_t ARRAY_ROWS>
void EigenVectorToArray(const Eigen::Matrix<T, ROWS, 1>& in, std::array<T, ARRAY_ROWS>& out) {
  assert(in.rows() == ARRAY_ROWS);
  std::copy(in.data(), in.data()+in.rows(), out.data());
}

template<int ROWS, typename T>
void EigenVectorToVector(const Eigen::Matrix<T, ROWS, 1>& in, std::vector<T>& out) {
  out.resize(in.size());
  std::copy(in.data(), in.data()+in.size(), out.data());
}

template<typename T, int ROWS, int COLS, size_t R, size_t C>
void EigenMatrixToArray(const Eigen::Matrix<T, ROWS, COLS>& in, std::array< std::array<T, R>, C>& out) {
  assert(in.rows() == ROWS);
  assert(in.cols() == COLS);
  static_assert(ROWS == R, "EigenMatrixToArray() : does not match Eigen::Matrix ROWS and std::array ROWS.");
  static_assert(COLS == C, "EigenMatrixToArray() : does not match Eigen::Matrix COLS and std::array COLS.");
  for (int i = 0; i < in.rows(); ++i) {
    for (int j = 0; j < in.cols(); ++j) {
      out[i][j] = in(i,j);
    }
  }
}

#endif // EIGEN_CONVERSIONS_H