#ifndef NUMERIC_TYPEDEFS_H
#define NUMERIC_TYPEDEFS_H

#include "EigenCore.h"
#include <Eigen/Dense>

typedef Eigen::VectorXf VectorXf;
typedef Eigen::MatrixXf MatrixXf;
typedef Eigen::Affine3f Affine3f;
typedef Eigen::Vector3f Vector3f;
typedef Eigen::Matrix3f Matrix3f;

#endif // NUMERIC_TYPEDEFS_H