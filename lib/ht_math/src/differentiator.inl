template <typename T>
Differentiator<T>::Differentiator(float sampling_time, const T& init_in):
  sampling_time_(sampling_time),
  filter_(sampling_time, init_in),
  pre_in_(init_in) {}

template <typename T>
T Differentiator<T>::Apply(const T& in) {
  T delta = (in - pre_in_) / sampling_time_;
  pre_in_ = in;
  return filter_.Apply(delta);
}

template <typename T>
const T& Differentiator<T>::Get() const {
  return pre_in_;
}

template <typename T>
void Differentiator<T>::SetCutoffFrequency(float frequency) {
  filter_.SetCutoffFrequency(frequency);
}