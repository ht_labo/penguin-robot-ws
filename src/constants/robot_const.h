#ifndef ROBOT_CONST_H
#define ROBOT_CONST_H

/**
 * @brief robot define macro
 */

// robot type
#define FLAPPING_WING     0
#define PENGUIN_ROBOT     1
#define MOTOR_TEST_BENCH  2
#define DIFF_GEAR_WING    3
#define ROBOT_TYPE        DIFF_GEAR_WING
inline int GetRobotType() {
  return ROBOT_TYPE;
}

// control type
#define SERVO_POS_FF  0
#define SERVO_VEL_FB  1
#define MAINTE        2
#define SERVO_POS_FB  3
#define CTRL_TYPE     SERVO_POS_FB

inline int GetCtrlMode() {
  return CTRL_TYPE;
}

// num const
#define NUM_JOINT 3 // num of joints per arm

// robot index
enum ArmIndex {
  RIGHT,
  LEFT,
  NUM_ARM
};

enum ControlMode {
  FK,
  IK,
  EFFORT,
  ACT_EFFORT,
  OPEN_LOOP,
  MS,
  CSV
};

enum PoseIndex {
  FLAP,
  FEATHER,
  LEAD_LAG,
  NUM_POSE
};

#define IO_MODULE_CYCLE_TIME_MS 3
#define CONTROLLER_CYCLE_TIME_MS 6
#define MAINTE_COM_CYCLE_TIME_MS 12

#endif // ROBOT_CONST_H