#include "penguin_kinematics.h"
#include "mechanical_model.h"
#include "../constants/robot_const.h"

VectorXf PenguinKinematics::PoseFromAngle(const VectorXf& q) {
  VectorXf ret(PoseIndex::NUM_POSE);
  ret[PoseIndex::LEAD_LAG] = q[0];
  CvtDifferentialGear(q[1], q[2], 1.0f, 1.0f, ret[PoseIndex::FLAP], ret[PoseIndex::FEATHER]);
  return ret;
}

VectorXf PenguinKinematics::PoseToAngle(const VectorXf& pose_q) {
  VectorXf ret(NUM_JOINT);
  ret[0] = pose_q[PoseIndex::LEAD_LAG];
  CvtInvDifferentialGear(pose_q[PoseIndex::FLAP], pose_q[PoseIndex::FEATHER], 1.0f, 1.0f, ret[1], ret[2]);
  return ret;
}