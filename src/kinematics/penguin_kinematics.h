#ifndef PENGUIN_KINEMATICS
#define PENGUIN_KINEMATICS

#include "numeric_typedefs.h"

class PenguinKinematics {
  public:
  static VectorXf PoseFromAngle(const VectorXf& q);
  static VectorXf PoseToAngle(const VectorXf& pose_q);
};

#endif // PENGUIN_KINEMATICS