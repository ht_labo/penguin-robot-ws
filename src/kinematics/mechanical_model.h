#ifndef MECHANICAL_MODEL
#define MECHANICAL_MODEL

inline void CvtDifferentialGear(float in_0, float in_1, float z_in, float z_out, float& out_0, float& out_1) {
  out_0 = (in_0 + in_1) * 0.5f;
  out_1 = (in_0 - in_1) * 0.5f * z_in / z_out;
}

inline void CvtInvDifferentialGear(float in_0, float in_1, float z_in, float z_out, float& out_0, float& out_1) {
  out_0 = in_0 + in_1 * z_out / z_in;
  out_1 = in_0 - in_1 * z_out / z_in;
}

#endif // MECHANICAL_MODEL