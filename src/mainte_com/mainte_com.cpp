#include "mainte_com.h"

#include "../control_data/extern_definition.h"
#include "../control_packet/mainte_packet.h"
#include "../control_packet/ipc_packet.h"
#include "../control_packet/log_msgpack.h"

static IPAddress MAINTE_IP(10, 10, 10, 1);
constexpr unsigned int MAINTE_TCP_PORT = 40000;
constexpr unsigned int MAINTE_UDP_PORT = 50000;
static IPAddress LOCAL_IP(10, 10, 10, 11);
constexpr unsigned int LOCAL_PORT = 50000;

ShareMemory<RobotData> robot_data_memory_;

/**
 * @brief Global parameter
 */
Queue mainte_to_controller_queue_(10, GetMaxMsgSize());
Queue controller_to_mainte_queue_(10, GetMaxMsgSize());

MainteCom::MainteCom() :
  clock_(MAINTE_COM_CYCLE_TIME_MS),
  counter_(0),
  is_tcp_server_connected_(false) {}

void MainteCom::LaunchThread(void* arg) {
  // start thread
  reinterpret_cast<MainteCom*>(arg)->Thread();
}

void MainteCom::Thread() {
  Init();
  while(true) {
    // cycle period
    clock_.Wait();
    // main process
    Execute();
    // update
    counter_++;
  }
}

void MainteCom::Init() {
  Serial.println("MainteCom : Init");
  Ethernet.begin(LOCAL_IP);
  udp_client_.begin(LOCAL_PORT);
  tcp_client_.setTimeout(2);
}

void MainteCom::Execute() {
  // read from shared memory
  RobotData robot_data;
  if (robot_data_memory_.Read(robot_data, 0)) {
    /* Udp */
    RobotMsg robot_msg;
    RobotDataToMsg(robot_data, robot_msg);
    SendMsgpack(robot_msg);
  }
  /* Thread Queue */
  ReactReceivedMsg();
  /* TCP */
  if (!is_tcp_server_connected_ && tcp_client_.connect(MAINTE_IP, MAINTE_TCP_PORT)) {
    is_tcp_server_connected_ = true;
    Serial.print("MainteServer Connected ");Serial.println(counter_);
  }
  if (is_tcp_server_connected_) {
    if (tcp_client_.available()) {
      uint32_t size = GetMaxPacketSize();
      uint8_t rx_buffer[size];
      int length = tcp_client_.read(rx_buffer, size);
      if (length > 0) ReactReceiveTcpPacket(rx_buffer);
    }
  }
  if (is_tcp_server_connected_ && !tcp_client_.connected()) {
    Serial.print("MainteServer Disconnected ");Serial.println(counter_);
    tcp_client_.stop();
    is_tcp_server_connected_ = false;
  }
}

template <typename T>
void MainteCom::SendMsgpack(const T& msgpack) {
  udp_client_.beginPacket(MAINTE_IP, MAINTE_UDP_PORT);
  // serialize
  MsgPack::Packer packer;
  packer.serialize(msgpack);
  // send
  udp_client_.write(packer.data(), packer.size());
  udp_client_.endPacket();
}

void MainteCom::ReactReceivedMsg() {
  const uint32_t num_of_msg = controller_to_mainte_queue_.NumOfItems();
  for (uint32_t i = 0; i < num_of_msg; i++) {
    uint8_t buf[GetMaxMsgSize()];
    controller_to_mainte_queue_.Receive(buf);
    uint8_t type = GetMsgType(buf);
    switch (type) {
      case MsgCmdType::MSG_ROBOT_TO_MAINTE_CSV_APPEND_RES: {
        MsgCmdCsvAppendRes cmd(buf);
        PacketCsvAppendRes packet(cmd.result);
        tcp_client_.write(reinterpret_cast<uint8_t*>(&packet), sizeof(PacketCsvAppendRes));
        break;
      }
    }
  }
}

void MainteCom::ReactReceiveTcpPacket(const void* buf) {
  uint8_t type = GetPacketType(buf);
  switch (type) {
    case PacketType::PACKET_SERVER_TO_ROBOT_CONTROL_ON: {
      PacketControl packet(buf);
      MsgCmdControl msg(MsgCmdType::MSG_MAINTE_TO_ROBOT_CONTROL_ON, packet.arm_id, packet.config);
      if (!mainte_to_controller_queue_.Send(&msg)) {
        Serial.print("MainteCom : Fail to Send MSG_MAINTE_TO_ROBOT_CONTROL_ON");
      }
      break;
    }
    case PacketType::PACKET_SERVER_TO_ROBOT_CONTROL_OFF: {
      PacketControl packet(buf);
      MsgCmdControl msg(MsgCmdType::MSG_MAINTE_TO_ROBOT_CONTROL_OFF, packet.arm_id, packet.config);
      if (!mainte_to_controller_queue_.Send(&msg, 0)) {
        Serial.print("MainteCom : Fail to Send MSG_MAINTE_TO_ROBOT_CONTROL_OFF");
      }
      break;
    }
    case PacketType::PACKET_SERVER_TO_ROBOT_CALIBRATION: {
      PacketCalibration packet(buf);
      MsgCmdCalibration msg(packet.arm_id, packet.joint_index);
      if (!mainte_to_controller_queue_.Send(&msg, 0)) {
        Serial.print("MainteCom : Fail to Send MSG_MAINTE_TO_ROBOT_CALIBRATION");
      }
      break;
    }
    case PacketType::PACKET_SERVER_TO_ROBOT_ALL_CONTROL_ON: {
      PacketAllControl packet(buf);
      MsgCmdAllControl msg(MsgCmdType::MSG_MAINTE_TO_ROBOT_ALL_CONTROL_ON, packet.operation_time, packet.config);
      if (!mainte_to_controller_queue_.Send(&msg)) {
        Serial.print("MainteCom : Fail to Send MSG_MAINTE_TO_ROBOT_ALL_CONTROL_ON");
      }
      break;
    }
    case PacketType::PACKET_SERVER_TO_ROBOT_ALL_CONTROL_OFF: {
      PacketAllControl packet(buf);
      MsgCmdAllControl msg(MsgCmdType::MSG_MAINTE_TO_ROBOT_ALL_CONTROL_OFF, packet.operation_time, packet.config);
      if (!mainte_to_controller_queue_.Send(&msg)) {
        Serial.print("MainteCom : Fail to Send MSG_MAINTE_TO_ROBOT_ALL_CONTROL_OFF");
      }
      break;
    }
    case PacketType::PACKET_SERVER_TO_ROBOT_CSV_APPEND_REQ: {
      PacketCsvAppendReq packet(buf);
      MsgCmdCsvAppendReq msg(packet.index, packet.csv);
      if (!mainte_to_controller_queue_.Send(&msg)) {
        Serial.print("MainteCom : Fail to Send MSG_MAINTE_TO_ROBOT_CSV_APPEND_REQ");
      }
      break;
    }
    case PacketType::PACKET_SERVER_TO_ROBOT_CSV_RESET: {
      Packet packet(buf);
      MsgCmd msg(MsgCmdType::MSG_MAINTE_TO_ROBOT_CSV_RESET);
      if (!mainte_to_controller_queue_.Send(&msg)) {
        Serial.print("MainteCom : Fail to Send MSG_MAINTE_TO_ROBOT_CSV_RESET");
      }
      break;
    }
  }
}