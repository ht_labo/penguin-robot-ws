#ifndef MAINTE_COM_H
#define MAINTE_COM_H
#include "../ipc/thread.hpp"
#include <LwIP.h>
#include <STM32Ethernet.h>
#include <EthernetUdp.h> 

class MainteCom {
  public:
  MainteCom();
  static void LaunchThread(void* arg);
  private:
  void Thread();
  void Init();
  void Execute();
  ThreadClock clock_;
  uint64_t counter_;
  // Unique
  EthernetUDP udp_client_;    // start as udp client
  EthernetClient tcp_client_;  // start ad tcp client
  template <typename T>
  void SendMsgpack(const T& msgpack);
  void ReactReceivedMsg();
  void ReactReceiveTcpPacket(const void* buf);
  bool is_tcp_server_connected_;
};

#endif // MAINTE_COM_H