#include "one_shot_timer.h"

OneShotTimer::OneShotTimer() :
  trigger_time_count_(0),
  has_trrigered_(true),
  count_(0) {}

void OneShotTimer::Start(uint32_t sample_time_ms, uint32_t time_ms) {
  trigger_time_count_ = time_ms / sample_time_ms;
  Reset();
}

void OneShotTimer::Reset() {
  has_trrigered_ = false;
  count_ = 0;
}

bool OneShotTimer::Update() {
  if (has_trrigered_) {
    return false;
  } else {
    count_++;
    if (count_ > trigger_time_count_) {
      has_trrigered_ = true;
      return true;
    } else {
      return false;
    }
  }
}