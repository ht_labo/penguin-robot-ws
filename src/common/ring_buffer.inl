template <typename T>
RingBuffer<T>::RingBuffer() :
  peek_index_(0) {}

template <typename T>
void RingBuffer<T>::Push(const T& data) {
  buffer_.push_back(data);
}

template <typename T>
const T& RingBuffer<T>::Peek() const {
  return buffer_.front();
}

template <typename T>
T& RingBuffer<T>::PeekNext() {
  T& ret = buffer_[peek_index_];
  // update index
  peek_index_++;
  if (peek_index_ >= buffer_.size()) peek_index_ = 0;
  return ret;
}

template <typename T>
void RingBuffer<T>::Reset() {
  buffer_.clear();
  peek_index_ = 0;
}

template <typename T>
size_t RingBuffer<T>::Length() {
  return buffer_.size();
}

template <typename T>
void RingBuffer<T>::SetIndex(unsigned int index) {
  peek_index_ = index;
}