#ifndef ONE_SHOT_TIMER
#define ONE_SHOT_TIMER

#include <stdint.h>

class OneShotTimer {
  public:
  OneShotTimer();
  void Start(uint32_t sample_time_ms, uint32_t time_ms);
  void Reset();
  bool Update();
  private:
  uint32_t trigger_time_count_;
  bool has_trrigered_;
  uint32_t count_;
};

#endif // ONE_SHOT_TIMER