#ifndef RING_BUFFER_H
#define RING_BUFFER_H

#include <vector>

template<typename T>
class RingBuffer {
  public:
  RingBuffer();
  void Push(const T& data);
  const T& Peek() const;
  T& PeekNext();
  void Reset();
  size_t Length();
  void SetIndex(unsigned int index);
  private:
  std::vector<T> buffer_;
  unsigned int peek_index_;
};

#include "ring_buffer.inl"

#endif // RING_BUFFER_H