#include "log_msgpack.h"
#include "eigen_conversions.h"

void InputDataToMsg(const InputData& in, InputMsg& out) {
  out.count = in.count;
  out.lap_time = in.lap_time;
  out.encoder_binary = in.encoder_binary;
  out.motor_binary = in.motor_binary;
}

void OutputDataToMsg(const OutputData& in, OutputMsg& out) {
  out.motor_binary = in.motor_binary;
  out.motor_enable = in.motor_enable;
}

void JointDataToMsg(const JointData& in, JointMsg& out) {
  EigenVectorToVector(in.q, out.q);
  EigenVectorToVector(in.qd, out.qd);
  EigenVectorToVector(in.qdd, out.qdd);
  EigenVectorToVector(in.effort, out.effort);
  EigenVectorToVector(in.effort_ff, out.effort_ff);
  EigenVectorToVector(in.act_q, out.act_q);
  EigenVectorToVector(in.act_qd, out.act_qd);
  EigenVectorToVector(in.act_effort, out.act_effort);
}

void StateDataToMsg(const StateData& in, StateMsg& out) {
  JointDataToMsg(in.joint, out.joint);
  EigenVectorToVector(in.pose_q, out.pose_q);
  EigenVectorToVector(in.pose_qd, out.pose_qd);
  EigenVectorToVector(in.pose_effort, out.pose_effort);
}

void RefDataToMsg(const RefData& in, RefMsg& out) {
  JointDataToMsg(in.joint, out.joint);
  EigenVectorToVector(in.pose_q, out.pose_q);
  EigenVectorToVector(in.pose_qd, out.pose_qd);
  EigenVectorToVector(in.pose_effort, out.pose_effort);
}

void ArmDataToMsg(const ArmData& in, ArmMsg& out) {
  InputDataToMsg(in.input, out.input);
  OutputDataToMsg(in.output, out.output);
  StateDataToMsg(in.state, out.state);
  RefDataToMsg(in.ref, out.ref);
}

void RobotDataToMsg(const RobotData& in, RobotMsg& out) {
  ArmDataToMsg(in.right, out.right);
  ArmDataToMsg(in.left, out.left);
}