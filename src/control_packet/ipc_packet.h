#ifndef IPC_PACKET_H
#define IPC_PACKET_H

#include <algorithm>
#include <stdint.h>
#include <memory.h> 
#include <vector>
#include "../control_data/control_config.h"
#include "../control_data/csv_format.h"

enum MsgCmdType {
  MSG_UNKOWN,
  // mainte to robot
  MSG_MAINTE_TO_ROBOT_CONTROL_ON,
  MSG_MAINTE_TO_ROBOT_CONTROL_OFF,
  MSG_MAINTE_TO_ROBOT_CALIBRATION,
  MSG_MAINTE_TO_ROBOT_ALL_CONTROL_ON,
  MSG_MAINTE_TO_ROBOT_ALL_CONTROL_OFF,
  MSG_MAINTE_TO_ROBOT_CSV_APPEND_REQ,
  MSG_MAINTE_TO_ROBOT_CSV_RESET,
  // robot to mainte
  MSG_ROBOT_TO_MAINTE_ROBOT_INFO,
  MSG_ROBOT_TO_MAINTE_CSV_APPEND_RES,
  // terminate
  MSG_ALL_TERMINATE
};

#pragma pack(push, 1)
struct MsgHeader {
  uint32_t size;
  uint8_t type;
  MsgHeader(uint32_t size = 0, uint8_t type = 0) : 
    size(size),
    type(type) {}
};

struct MsgCmd {
  MsgHeader header;
  MsgCmd(uint8_t type = 0) :
    header(sizeof(MsgCmd), type) {}
  MsgCmd(const void* buf) {memmove(this, buf, sizeof(*this));}
};

struct MsgCmdControl {
  MsgHeader header;
  uint8_t arm_id;
  ControlConfig config;
  MsgCmdControl(uint8_t type = 0, uint8_t arm_id = 0, ControlConfig config = ControlConfig()) : 
    header(sizeof(MsgCmdControl), type),
    arm_id(arm_id),
    config(config) {}
  MsgCmdControl(const void* buf) {memmove(this, buf, sizeof(*this));}
};

struct MsgCmdCalibration {
  MsgHeader header;
  uint8_t arm_id;
  uint8_t joint_index;
  MsgCmdCalibration(uint8_t arm_id = 0, uint8_t joint_index = 0) : 
    header(sizeof(MsgCmdCalibration), MsgCmdType::MSG_MAINTE_TO_ROBOT_CALIBRATION),
    arm_id(arm_id),
    joint_index(joint_index) {}
  MsgCmdCalibration(const void* buf) {memmove(this, buf, sizeof(*this));}
};

struct MsgCmdAllControl {
  MsgHeader header;
  uint32_t operation_time;
  std::array<ControlConfig, ArmIndex::NUM_ARM> config;
  MsgCmdAllControl(uint8_t type, uint32_t time, std::array<ControlConfig, ArmIndex::NUM_ARM>& config) : 
    header(sizeof(MsgCmdAllControl), type),
    operation_time(time),
    config(config) {}
  MsgCmdAllControl(const void* buf) {memmove(this, buf, sizeof(*this));}
};

struct MsgCmdCsvAppendReq {
  MsgHeader header;
  uint32_t index;
  CsvFormat csv;
  MsgCmdCsvAppendReq(uint32_t index, const CsvFormat& csv) :
    header(sizeof(MsgCmdCsvAppendReq), MsgCmdType::MSG_MAINTE_TO_ROBOT_CSV_APPEND_REQ),
    index(index),
    csv(csv) {}
  MsgCmdCsvAppendReq(const void* buf) {memmove(this, buf, sizeof(*this));}
};

struct MsgCmdCsvAppendRes {
  MsgHeader header;
  bool result;
  MsgCmdCsvAppendRes(bool result) :
    header(sizeof(MsgCmdCsvAppendRes), MsgCmdType::MSG_ROBOT_TO_MAINTE_CSV_APPEND_RES),
    result(result) {}
  MsgCmdCsvAppendRes(const void* buf) {memmove(this, buf, sizeof(*this));}
};

#pragma pack(pop)

inline uint8_t GetMsgType(const void* buf) {
  MsgHeader header;
  memmove(&header, buf, sizeof(header));
  return header.type;
}

inline uint32_t GetMaxMsgSize() {
  const std::vector<size_t> msg_size_array {
    sizeof(MsgCmd),
    sizeof(MsgCmdControl),
    sizeof(MsgCmdCalibration),
    sizeof(MsgCmdAllControl),
    sizeof(MsgCmdCsvAppendReq),
    sizeof(MsgCmdCsvAppendRes)
  };
  return *std::max_element(msg_size_array.begin(), msg_size_array.end());
}

#endif // IPC_PACKET_H