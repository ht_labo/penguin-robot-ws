#ifndef MAINTE_PACKET_H
#define MAINTE_PACKET_H

#include <algorithm>
#include <stdint.h>
#include <memory.h> 
#include <vector>
#include "../control_data/control_config.h"
#include "../control_data/csv_format.h"

enum PacketType {
  PACKET_UNKOWN,
  // server to robot
  PACKET_SERVER_TO_ROBOT_CONTROL_ON,
  PACKET_SERVER_TO_ROBOT_CONTROL_OFF,
  PACKET_SERVER_TO_ROBOT_CALIBRATION,
  PACKET_SERVER_TO_ROBOT_ALL_CONTROL_ON,
  PACKET_SERVER_TO_ROBOT_ALL_CONTROL_OFF,
  PACKET_SERVER_TO_ROBOT_CSV_APPEND_REQ,
  PACKET_SERVER_TO_ROBOT_CSV_RESET,
  // robot to mainte
  PACKET_ROBOT_TO_SERVER_ROBOT_INFO,
  PACKET_ROBOT_TO_SERVER_CSV_APPEND_RES,
  // terminate
  PACKET_TERMINATE
};

#pragma pack(push, 1)
struct TcpHeader {
  uint32_t size;
  uint8_t type;
  TcpHeader(uint32_t size = 0, uint8_t type = 0) : 
    size(size),
    type(type) {}
};

struct Packet {
  TcpHeader header;
  Packet(uint8_t type = 0) :
    header(sizeof(Packet), type) {}
  Packet(const void* buf) {memmove(this, buf, sizeof(*this));}
};

struct PacketControl {
  TcpHeader header;
  uint8_t arm_id;
  ControlConfig config;
  PacketControl(uint8_t type = 0, uint8_t arm_id = 0, ControlConfig config = ControlConfig()) : 
    header(sizeof(PacketControl), type),
    arm_id(arm_id),
    config(config) {}
  PacketControl(const void* buf) {memmove(this, buf, sizeof(*this));}
};

struct PacketCalibration {
  TcpHeader header;
  uint8_t arm_id;
  uint8_t joint_index;
  PacketCalibration(uint8_t arm_id = 0, uint8_t joint_index = 0) : 
    header(sizeof(PacketCalibration), PACKET_SERVER_TO_ROBOT_CALIBRATION),
    arm_id(arm_id),
    joint_index(joint_index) {}
  PacketCalibration(const void* buf) {memmove(this, buf, sizeof(*this));}
};

struct PacketAllControl {
  TcpHeader header;
  uint32_t operation_time;
  std::array<ControlConfig, ArmIndex::NUM_ARM> config;
  PacketAllControl(uint8_t type, uint8_t time, std::array<ControlConfig, ArmIndex::NUM_ARM>& config) : 
    header(sizeof(PacketAllControl), type),
    operation_time(time),
    config(config) {}
  PacketAllControl(const void* buf) {memmove(this, buf, sizeof(*this));}
};

struct PacketCsvAppendReq {
  TcpHeader header;
  uint32_t index;
  CsvFormat csv;
  PacketCsvAppendReq(uint32_t index, const CsvFormat& csv) :
    header(sizeof(PacketCsvAppendReq), PACKET_SERVER_TO_ROBOT_CSV_APPEND_REQ),
    index(index),
    csv(csv) {}
  PacketCsvAppendReq(const void* buf) {memmove(this, buf, sizeof(*this));}
};

struct PacketCsvAppendRes {
  TcpHeader header;
  bool result;
  PacketCsvAppendRes(bool result) :
    header(sizeof(PacketCsvAppendRes), PACKET_ROBOT_TO_SERVER_CSV_APPEND_RES),
    result(result) {}
  PacketCsvAppendRes(const void* buf) {memmove(this, buf, sizeof(*this));}
};

#pragma pack(pop)

inline uint8_t GetPacketType(const void* buf) {
  TcpHeader header;
  memmove(&header, buf, sizeof(header));
  return header.type;
}

inline uint32_t GetMaxPacketSize() {
  const std::vector<size_t> packet_size_array {
    sizeof(Packet),
    sizeof(PacketControl),
    sizeof(PacketCalibration),
    sizeof(PacketAllControl),
    sizeof(PacketCsvAppendReq),
    sizeof(PacketCsvAppendRes)
  };
  return *std::max_element(packet_size_array.begin(), packet_size_array.end());
}

#endif // MAINTE_PACKET_H