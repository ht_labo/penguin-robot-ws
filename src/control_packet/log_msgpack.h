#ifndef LOG_MSG_PACK_H
#define LOG_MSG_PACK_H
#include <MsgPack.h>
#include "../control_data/robot_data.h"

struct InputMsg : InputData {
  MsgPack::str_t key_count{"count"};
  MsgPack::str_t key_lap_time{"lap_time"};
  MsgPack::str_t key_encoder_binary{"encoder_binary"};
  MsgPack::str_t key_motor_binary{"motor_binary"};
  MSGPACK_DEFINE_MAP(key_count, count, key_lap_time, lap_time, key_encoder_binary, encoder_binary, key_motor_binary, motor_binary);
};

struct OutputMsg : OutputData {
  MsgPack::str_t key_motor_binary{"motor_binary"};
  MsgPack::str_t key_motor_enable{"motor_enable"};
  MSGPACK_DEFINE_MAP(key_motor_binary, motor_binary, key_motor_enable, motor_enable);
};

struct JointMsg {
  std::vector<float> q;
  std::vector<float> qd;
  std::vector<float> qdd;
  std::vector<float> effort;
  std::vector<float> effort_ff;
  std::vector<float> act_q;
  std::vector<float> act_qd;
  std::vector<float> act_effort;
  explicit JointMsg(uint8_t num_of_joints = 0) {
    q.resize(num_of_joints);
    qd.resize(num_of_joints);
    qdd.resize(num_of_joints);
    effort.resize(num_of_joints);
    effort_ff.resize(num_of_joints);
    act_q.resize(num_of_joints);
    act_qd.resize(num_of_joints);
    act_effort.resize(num_of_joints);
  }
  MsgPack::str_t key_q{"q"};
  MsgPack::str_t key_qd{"qd"};
  //MsgPack::str_t key_qd{"qdd"};
  MsgPack::str_t key_effort{"effort"};
  MsgPack::str_t key_effort_ff{"effort_ff"};
  //MsgPack::str_t key_act_q{"act_q"};
  //MsgPack::str_t key_act_qd{"act_qd"};
  //MsgPack::str_t key_act_effort{"act_effort"};
  MSGPACK_DEFINE_MAP(key_q, q, key_qd, qd, key_effort, effort, key_effort_ff, effort_ff);
  //MSGPACK_DEFINE_MAP(key_q, q, key_qd, qd, key_effort, effort, key_act_q, act_q, key_act_qd, act_qd, key_act_effort, act_effort);
};

struct StateMsg {
  JointMsg joint;
  std::vector<float> pose_q;
  std::vector<float> pose_qd;
  std::vector<float> pose_effort;
  StateMsg():
    joint(NUM_JOINT) {
    pose_q.resize(XYZ);
    pose_qd.resize(XYZ);
    pose_effort.resize(XYZ);
  }
  MsgPack::str_t key_joint{"joint"};
  MSGPACK_DEFINE_MAP(key_joint, joint);
  //MsgPack::str_t key_pose_q{"pose_q"};
  //MsgPack::str_t key_pose_qd{"pose_qd"};
  //MsgPack::str_t key_pose_effort{"pose_effort"};
  //MSGPACK_DEFINE_MAP(key_joint, joint, key_pose_q, pose_q, key_pose_qd, pose_qd, key_pose_effort, pose_effort);
};

struct RefMsg {
  JointMsg joint;
  std::vector<float> pose_q;
  std::vector<float> pose_qd;
  std::vector<float> pose_effort;
  RefMsg():
    joint(NUM_JOINT) {
    pose_q.resize(XYZ);
    pose_qd.resize(XYZ);
    pose_effort.resize(XYZ);
  }
  MsgPack::str_t key_joint{"joint"};
  MSGPACK_DEFINE_MAP(key_joint, joint);
  //MsgPack::str_t key_pose_q{"pose_q"};
  //MsgPack::str_t key_pose_qd{"pose_qd"};
  //MsgPack::str_t key_pose_effort{"pose_effort"};
  //MSGPACK_DEFINE_MAP(key_joint, joint, key_pose_q, pose_q, key_pose_qd, pose_qd, key_pose_effort, pose_effort);
};

struct ArmMsg {
  InputMsg   input;
  OutputMsg  output;
  StateMsg   state;
  RefMsg     ref;
  MsgPack::str_t key_input{"input"};
  MsgPack::str_t key_output{"output"};
  MsgPack::str_t key_state{"state"};
  MsgPack::str_t key_ref{"ref"};
  MSGPACK_DEFINE_MAP(key_input, input, key_output, output, key_state, state, key_ref, ref);
};

struct RobotMsg {
  ArmMsg right;
  ArmMsg left;
  MsgPack::str_t key_right{"right"};
  MsgPack::str_t key_left{"left"};
  MSGPACK_DEFINE_MAP(key_right, right, key_left, left);
};

void InputDataToMsg(const InputData& in, InputMsg& out);
void OutputDataToMsg(const OutputData& in, OutputMsg& out);
void JointDataToMsg(const JointData& in, JointMsg& out);
void StateDataToMsg(const StateData& in, StateMsg& out);
void RefDataToMsg(const RefData& in, RefMsg& out);
void ArmDataToMsg(const ArmData& in, ArmMsg& out);
void RobotDataToMsg(const RobotData& in, RobotMsg& out);

#endif // LOG_MSG_PACK_H