#include "thread.hpp"
#include <Arduino.h>

Thread::Thread() {

}

void Thread::Start(void (*function)(void*), const std::string& name, uint8_t priority, uint32_t stack_size, void* arg) {
    xTaskCreate(function, name.c_str(), stack_size, arg, priority, &task_handle_);
}

uint32_t Thread::GetStackMargin() {
    return uxTaskGetStackHighWaterMark(task_handle_);
}

ThreadClock::ThreadClock(uint32_t ms) {
    previous_wake_time_ = xTaskGetTickCount();
    wait_ms_ = ms;
}

void ThreadClock::Wait() {
    vTaskDelayUntil(&previous_wake_time_, wait_ms_ / portTICK_RATE_MS);
}

void ThreadClock::Reset() {
    previous_wake_time_ = xTaskGetTickCount();
}

StopWatch::StopWatch() {
    time_offset_ = 0;
}

void StopWatch::Reset() {
    time_offset_ = micros();
}

uint64_t StopWatch::GetTime() {
    return micros() - time_offset_;
}