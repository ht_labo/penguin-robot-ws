#include "semaphore.hpp"

Semaphore::Semaphore() {
    x_semaphore_ = xSemaphoreCreateBinary();
}

bool Semaphore::Take(TickType_t timeout_tick) {
    if (xSemaphoreTake(x_semaphore_, timeout_tick) == pdTRUE) {
        return true;
    } else {
        return false;
    }
}

bool Semaphore::Give() {
    if (xSemaphoreGive(x_semaphore_) == pdTRUE) {
        return true;
    } else {
        return false;
    }
}

uint32_t Semaphore::GetCount() {
    return static_cast<uint32_t>(uxSemaphoreGetCount(x_semaphore_));
}