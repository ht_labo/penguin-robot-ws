#ifndef X_SEMAPHORE_H
#define X_SEMAPHORE_H

#include <STM32FreeRTOS.h>

class Semaphore {
    public:
    Semaphore();
    bool Take(TickType_t timeout_tick = portMAX_DELAY);
    bool Give();
    uint32_t GetCount();
    private:
    SemaphoreHandle_t x_semaphore_;
};

#endif