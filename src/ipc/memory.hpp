#ifndef X_MEMORY_H
#define X_MEMORY_H

#include <STM32FreeRTOS.h>

template<typename T>
class ShareMemory {
    public:
    ShareMemory();
    bool Write(const T& data, TickType_t timeout_tick = portMAX_DELAY);
    bool Read(T& data, TickType_t timeout_tick = portMAX_DELAY);
    private:
    SemaphoreHandle_t x_mutex_;
    T memory_;
};

template <typename T>
ShareMemory<T>::ShareMemory() {
    x_mutex_ = xSemaphoreCreateMutex();
    memory_ = T();
}

template <typename T>
bool ShareMemory<T>::Write(const T& data, TickType_t timeout_tick) {
    if (xSemaphoreTake(x_mutex_, timeout_tick) == pdTRUE) {
        memory_ = data;
        xSemaphoreGive(x_mutex_);
        return true;
    } else {
        return false;
    }
}

template <typename T>
bool ShareMemory<T>::Read(T& data, TickType_t timeout_tick) {
    if (xSemaphoreTake(x_mutex_, timeout_tick) == pdTRUE) {
        data = memory_;
        xSemaphoreGive(x_mutex_);
        return true;
    } else {
        return false;
    }
}

#endif