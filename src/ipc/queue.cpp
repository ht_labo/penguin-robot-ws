#include "queue.hpp"

Queue::Queue(uint32_t length, uint32_t size) {
    x_queue_ = xQueueCreate(length, size);
}

bool Queue::Send(const void* data, TickType_t timeout_tick) {
    if (xQueueSendToBack(x_queue_, data, timeout_tick) == pdTRUE) {
        return true;
    } else {
        return false;
    }
}

bool Queue::Receive(void* data, TickType_t timeout_tick) {
    if (xQueueReceive(x_queue_, data, timeout_tick) == pdTRUE) {
        return true;
    } else {
        return false;
    }
}

uint32_t Queue::NumOfItems() {
    return uxQueueMessagesWaiting(x_queue_);
}

bool Queue::ReceivePeek(void* data, TickType_t timeout_tick) {
    if (xQueuePeek(x_queue_, data, timeout_tick) == pdTRUE) {
        return true;
    } else {
        return false;
    }
}