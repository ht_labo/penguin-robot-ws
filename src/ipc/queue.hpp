#ifndef X_QUEUE_H
#define X_QUEUE_H

#include <STM32FreeRTOS.h>

class Queue {
    public:
    Queue(uint32_t length, uint32_t size);
    bool Send(const void* data, TickType_t timeout_tick = portMAX_DELAY);
    bool Receive(void* data, TickType_t timeout_tick = portMAX_DELAY);
    uint32_t NumOfItems();
    bool ReceivePeek(void* data, TickType_t timeout_tick = portMAX_DELAY);
    private:
    QueueHandle_t x_queue_;
};

#endif