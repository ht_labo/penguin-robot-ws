#ifndef THREAD_H
#define THREAD_H

#include <STM32FreeRTOS.h>
#include <string>

class Thread {
    public:
    Thread();
    void Start(void (*function)(void*), const std::string& name, uint8_t priority, uint32_t stack_size, void* arg);
    uint32_t GetStackMargin();
    private:
    xTaskHandle task_handle_;
};

class ThreadClock {
    public:
    ThreadClock(uint32_t ms);
    void Wait();
    void Reset();
    private:
    portTickType previous_wake_time_;
    uint32_t wait_ms_;
};

class StopWatch {
    public:
    StopWatch();
    void Reset();
    uint64_t GetTime();
    private:
    uint64_t time_offset_;
};

class ThreadBase {
    public:
    ThreadBase();
    virtual void Task();
    private:
    virtual void Initialize();
    virtual void Excute();
    protected:
    ThreadClock clock_;
};

#endif