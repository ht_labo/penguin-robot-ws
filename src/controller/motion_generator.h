#ifndef MOTION_GENERATOR_H
#define MOTION_GENERATOR_H

#include "numeric_typedefs.h"
#include "wave.h"
#include <vector>
#include "../control_data/control_config.h"
#include "../control_data/arm_data.h"
#include "../control_data/ms_data.h"
#include "smooth_path.h"

enum State {
  INITIALIZE,
  STOP,
  RUN
};

class GeneratorBase {
  public:
  GeneratorBase();
  virtual ~GeneratorBase();
  virtual void Config(const Reference& reference);
  virtual void Update(float time, const JointData& state, const JointData& pre_ref, const MsData& ms_data, JointData& ref, OutputData& output);
  void StartRequest();
  void StopRequest();
  void CompleteConfig();
  protected:
  State state_;
};

class FKGenerator : public GeneratorBase {
  public:
  FKGenerator(uint8_t num_of_joints);
  ~FKGenerator();
  void Config(const Reference& reference);
  void Update(float time, const JointData& state, const JointData& pre_ref, const MsData& ms_data, JointData& ref, OutputData& output) override;
  private:
  std::vector<Wave> wave_;
  bool is_first_loop_;
  SmoothingPath<VectorXf> q_traj_;
};

class IKGenerator : public GeneratorBase {
  public:
  IKGenerator(uint8_t num_of_targets);
  ~IKGenerator();
  void Config(const Reference& reference);
  void Update(float time, const JointData& state, const JointData& pre_ref, const MsData& ms_data, JointData& ref, OutputData& output) override;
  private:
  std::vector<Wave> wave_;
  bool is_first_loop_;
  SmoothingPath<VectorXf> q_traj_;
};

class EffortGenerator : public GeneratorBase {
  public:
  EffortGenerator(uint8_t num_of_joints);
  ~EffortGenerator();
  void Config(const Reference& reference);
  void Update(float time, const JointData& state, const JointData& pre_ref, const MsData& ms_data, JointData& ref, OutputData& output) override;
  private:
  std::vector<Wave> wave_;
};

class ActEffortGenerator : public GeneratorBase {
  public:
  ActEffortGenerator(uint8_t num_of_joints);
  ~ActEffortGenerator();
  void Config(const Reference& reference);
  void Update(float time, const JointData& state, const JointData& pre_ref, const MsData& ms_data, JointData& ref, OutputData& output) override;
  private:
  std::vector<Wave> wave_;
};

class OpenloopGenerator : public GeneratorBase {
  public:
  OpenloopGenerator(uint8_t num_of_joints);
  ~OpenloopGenerator();
  void Config(const Reference& reference);
  void Update(float time, const JointData& state, const JointData& pre_ref, const MsData& ms_data, JointData& ref, OutputData& output) override;
  private:
  std::vector<Wave> wave_;
};

class MSGenerator : public GeneratorBase {
  public:
  MSGenerator(uint8_t num_of_joints);
  ~MSGenerator();
  void Config(const Reference& reference);
  void Update(float time, const JointData& state, const JointData& pre_ref, const MsData& ms_data, JointData& ref, OutputData& output) override;
};


#endif // MOTION_GENERATOR_H