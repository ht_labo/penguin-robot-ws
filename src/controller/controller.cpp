#include "controller.h"
#include "../control_data/extern_definition.h"
#include <Arduino.h>
#include "../control_packet/ipc_packet.h"

/**
 * @class Controller
 */
Controller::Controller() :
  clock_(CONTROLLER_CYCLE_TIME_MS),
  counter_(0) {
}

void Controller::Init() {
  Serial.println("Controller : Init");
  /* set config */
  // Hardware Config
  HardwareConfig hardware_config;
  GetHardwareConfig(hardware_config);
  arms_[ArmIndex::RIGHT].Init(hardware_config.right);
  arms_[ArmIndex::LEFT].Init(hardware_config.left);
  // Control Config
  ControlConfig control_config;
  GetGainConfig(control_config.gain);
  arms_[ArmIndex::RIGHT].Config(control_config);
  arms_[ArmIndex::LEFT].Config(control_config);
}

void Controller::Execute() {
  double time = counter_ * CONTROLLER_CYCLE_TIME_MS * MS_TO_S;
  /* Check Msg */
  ReactReceivedMsg();
  /* Check Timeout */
  CheckTimeOut();
  /* Arm */
  for (size_t i = 0; i < arms_.size(); i++) {
    arms_[i].Update(time);
  }
  /* Log */
  RobotData robot_data;
  robot_data.right = arms_[ArmIndex::RIGHT].RefArmData();
  robot_data.left = arms_[ArmIndex::LEFT].RefArmData();
  robot_data_memory_.Write(robot_data);
}

void Controller::ReactReceivedMsg() {
  const uint32_t num_of_msg = mainte_to_controller_queue_.NumOfItems();
  for (uint32_t i = 0; i < num_of_msg; i++) {
    uint8_t buf[GetMaxMsgSize()];
    mainte_to_controller_queue_.Receive(buf);
    uint8_t type = GetMsgType(buf);
    switch(type) {
      case MSG_MAINTE_TO_ROBOT_CONTROL_ON: {
        MsgCmdControl cmd(buf);
        ReactControlOn(cmd.arm_id, cmd.config);
        break;
      }
      case MSG_MAINTE_TO_ROBOT_CONTROL_OFF: {
        MsgCmdControl cmd(buf);
        ReactControlOff(cmd.arm_id);
        break;
      }
      case MSG_MAINTE_TO_ROBOT_CALIBRATION: {
        MsgCmdCalibration cmd(buf);
        ReactCalibration(cmd.arm_id, cmd.joint_index);
        break;
      }
      case MSG_MAINTE_TO_ROBOT_ALL_CONTROL_ON: {
        MsgCmdAllControl cmd(buf);
        Serial.print(" Operation Time : ");Serial.println(cmd.operation_time);
        control_off_timer_.Start(CONTROLLER_CYCLE_TIME_MS, cmd.operation_time * S_TO_MS);
        for (size_t i = 0; i < cmd.config.size(); i++) {
          ReactControlOn(i, cmd.config[i]);
        }
        break;
      }
      case MSG_MAINTE_TO_ROBOT_ALL_CONTROL_OFF: {
        MsgCmdAllControl cmd(buf);
        for (size_t i = 0; i < cmd.config.size(); i++) {
          ReactControlOff(i);
        }
        break;
      }
      case MSG_MAINTE_TO_ROBOT_CSV_APPEND_REQ: {
        MsgCmdCsvAppendReq cmd(buf);
        ReactAppendCsv(cmd.index, cmd.csv);
        break;
      }
      case MSG_MAINTE_TO_ROBOT_CSV_RESET: {
        ReactResetCsv();
      }
      default: {
        break;
      }
    }
  }
}

void Controller::ReactControlOn(int arm_id, const ControlConfig& config) {
  Serial.print("Controller ControlOn Arm : ");Serial.println(arm_id);
  Serial.print(" Mode : ");Serial.println(config.control_mode);
  arms_[arm_id].Config(config);
  arms_[arm_id].Start();
}

void Controller::ReactControlOff(int arm_id) {
  Serial.print("Controller ControlOff Arm : ");Serial.println(arm_id);
  arms_[arm_id].Stop();
}

void Controller::ReactCalibration(int arm_id, int joint_index) {
  Serial.print("Controller Calibration : ");Serial.print(arm_id);
  Serial.print(" Joint : ");Serial.println(joint_index);
  if (joint_index == NUM_JOINT) {
    // All Joint Calibration
    for (size_t i = 0; i < arms_[arm_id].RefArmData().state.joint.q.size(); i++) {
      arms_[arm_id].CalibrateOffset(i, arms_[arm_id].RefArmData().state.joint.q[i]);
    }
  } else {
    // Individual Joint Calibration
    arms_[arm_id].CalibrateOffset(joint_index, arms_[arm_id].RefArmData().state.joint.q[joint_index]);
  }
}

void Controller::ReactAppendCsv(uint32_t index, const CsvFormat csv) {
  Serial.println("Controller Append Csv");
  // Add to storage
  arms_[ArmIndex::RIGHT].AppendCsvData(csv.right);
  arms_[ArmIndex::LEFT].AppendCsvData(csv.left);

  // return response
  MsgCmdCsvAppendRes msg((index == 0) ? true : false);
  if (!controller_to_mainte_queue_.Send(&msg)) {
    Serial.print("Controller : Fail to Send MSG_MAINTE_TO_ROBOT_CSV_APPEND_RES");
  }
}

void Controller::ReactResetCsv() {
  Serial.println("Controller Reset Csv");
  // Reset csv storage
  arms_[ArmIndex::RIGHT].ResetCsvData();
  arms_[ArmIndex::LEFT].ResetCsvData();
}

void Controller::CheckTimeOut() {
  if (control_off_timer_.Update()) {
    for (size_t i = 0; i < arms_.size(); i++) {
      ReactControlOff(i);
    }
  }
}

void Controller::Thread() {
  Init();
  while(true) {
    /* Semaphore */
    if (true) {
        // synchronize with semaphore
        if(!sync_semaphore_.Take(500 / portTICK_RATE_MS)) {
            break;
        }
    } else {
      // synchronize with cycle period
      clock_.Wait();
    }
    // main process
    Execute();
    // update
    counter_++;
  }
  Serial.println("End Task");
  
}

void Controller::LaunchThread(void* arg) {
  // start thread
  reinterpret_cast<Controller*>(arg)->Thread();
}