#include "motion_generator.h"
#include "../kinematics/penguin_kinematics.h"
#include "eigen_conversions.h"

constexpr float SMOOTH_TIME = 0.0f;

/**
 * @class GeneratorBase
 */
GeneratorBase::GeneratorBase() :
  state_(State::INITIALIZE) {}

GeneratorBase::~GeneratorBase() {}

void GeneratorBase::Config(const Reference& reference) {}

void GeneratorBase::Update(float time, const JointData& state, const JointData& pre_ref, const MsData& ms_data, JointData& ref, OutputData& output) {}

void GeneratorBase::StartRequest() {
  if (state_ == State::STOP) {
    state_ = State::RUN;
  }
}

void GeneratorBase::StopRequest() {
  if ((state_ == State::INITIALIZE) || (state_ == State::RUN)) {
    state_ = State::STOP;
  }
}

void GeneratorBase::CompleteConfig() {
  if (state_ == State::INITIALIZE) {
    state_ = State::STOP;
  }
}

/**
 * @class FKGenerator
 */
FKGenerator::FKGenerator(uint8_t num_of_joints) :
  GeneratorBase(),
  wave_(num_of_joints) {}

FKGenerator::~FKGenerator() {}

void FKGenerator::Config(const Reference& reference) {
  for (size_t i = 0; i < wave_.size(); i++) {
    WaveForm form = reference.fk[i];
    wave_[i].Config(form.type, form.amplitude * DEG_TO_RAD, form.base * DEG_TO_RAD, form.frequency, form.phase * DEG_TO_RAD);
  }
  is_first_loop_ = true;
}

void FKGenerator::Update(float time, const JointData& state, const JointData& pre_ref, const MsData& ms_data, JointData& ref, OutputData& output) {
  if (state_ == State::RUN) {
    for (size_t i = 0; i < wave_.size(); i++) {
      ref.q[i] = wave_[i].Update(time);
    }
    if (is_first_loop_) {
      is_first_loop_ = false;
      q_traj_.Create(state.q, ref.q, time, SMOOTH_TIME);
    }

    if (q_traj_.DoSmoothing()) {
      q_traj_.ModifyTarget(ref.q);
      ref.q = q_traj_.Get(time);
    }
  } else {
    ref = state;
  }
}

/**
 * @class IKGenerator
 */
IKGenerator::IKGenerator(uint8_t num_of_targets) :
  GeneratorBase(),
  wave_(num_of_targets) {}

IKGenerator::~IKGenerator() {}

void IKGenerator::Config(const Reference& reference) {
  for (size_t i = 0; i < wave_.size(); i++) {
    WaveForm form = reference.ik[i];
    wave_[i].Config(form.type, form.amplitude * DEG_TO_RAD, form.base * DEG_TO_RAD, form.frequency, form.phase * DEG_TO_RAD);
  }
  is_first_loop_ = true;
}

void IKGenerator::Update(float time, const JointData& state, const JointData& pre_ref, const MsData& ms_data, JointData& ref, OutputData& output) {
  if (state_ == State::RUN) {
    VectorXf pose_q(wave_.size());
    for (size_t i = 0; i < wave_.size(); i++) {
      pose_q[i] = wave_[i].Update(time);
    }
    // cvt to joint q
    ref.q = PenguinKinematics::PoseToAngle(pose_q);

    if (is_first_loop_) {
      is_first_loop_ = false;
      q_traj_.Create(state.q, ref.q, time, SMOOTH_TIME);
    }

    if (q_traj_.DoSmoothing()) {
      q_traj_.ModifyTarget(ref.q);
      ref.q = q_traj_.Get(time);
    }
  } else {
    ref = state;
  }
}

/**
 * @class EffortGenerator
 */
EffortGenerator::EffortGenerator(uint8_t num_of_joints) :
  GeneratorBase(),
  wave_(num_of_joints) {}

EffortGenerator::~EffortGenerator() {}

void EffortGenerator::Config(const Reference& reference) {
  for (size_t i = 0; i < wave_.size(); i++) {
    WaveForm form = reference.effort[i];
    wave_[i].Config(form.type, form.amplitude, form.base, form.frequency, form.phase * DEG_TO_RAD);
  }
}

void EffortGenerator::Update(float time, const JointData& state, const JointData& pre_ref, const MsData& ms_data, JointData& ref, OutputData& output) {
  if (state_ == State::RUN) {
    for (size_t i = 0; i < wave_.size(); i++) {
      ref.effort[i] = wave_[i].Update(time);
    }
  }
}

/**
 * @class ActEffortGenerator
 */
ActEffortGenerator::ActEffortGenerator(uint8_t num_of_joints) :
  GeneratorBase(),
  wave_(num_of_joints) {}

ActEffortGenerator::~ActEffortGenerator() {}

void ActEffortGenerator::Config(const Reference& reference) {
  for (size_t i = 0; i < wave_.size(); i++) {
    WaveForm form = reference.act_effort[i];
    wave_[i].Config(form.type, form.amplitude, form.base, form.frequency, form.phase * DEG_TO_RAD);
  }
}

void ActEffortGenerator::Update(float time, const JointData& state, const JointData& pre_ref, const MsData& ms_data, JointData& ref, OutputData& output) {
  if (state_ == State::RUN) {
    for (size_t i = 0; i < wave_.size(); i++) {
      ref.act_effort[i] = wave_[i].Update(time);
    }
  }
}

/**
 * @class OpenloopGenerator
 */
OpenloopGenerator::OpenloopGenerator(uint8_t num_of_joints) :
  GeneratorBase(),
  wave_(num_of_joints) {}

OpenloopGenerator::~OpenloopGenerator() {}

void OpenloopGenerator::Config(const Reference& reference) {
  for (size_t i = 0; i < wave_.size(); i++) {
    WaveForm form = reference.openloop[i];
    wave_[i].Config(form.type, form.amplitude, form.base, form.frequency, form.phase * DEG_TO_RAD);
  }
}

void OpenloopGenerator::Update(float time, const JointData& state, const JointData& pre_ref, const MsData& ms_data, JointData& ref, OutputData& output) {
  if (state_ == State::RUN) {
    for (size_t i = 0; i < wave_.size(); i++) {
      output.motor_binary[i] = wave_[i].Update(time);
    }
  }
}

/**
 * @class MSGenerator
 */
MSGenerator::MSGenerator(uint8_t num_of_joints) :
  GeneratorBase() {}

MSGenerator::~MSGenerator() {}

void MSGenerator::Config(const Reference& reference) {
}

void MSGenerator::Update(float time, const JointData& state, const JointData& pre_ref, const MsData& ms_data, JointData& ref, OutputData& output) {
  if (state_ == State::RUN) {
    // cvt to joint q
    if (ms_data.active) {
      ref.q = PenguinKinematics::PoseToAngle(ArrayToEigenVector(ms_data.pose) * DEG_TO_RAD);
    } else {
      ref = pre_ref;
    }
  } else {
    ref = state;
  }
}