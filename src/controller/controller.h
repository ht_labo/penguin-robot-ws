#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "../ipc/thread.hpp"
#include "arm_controller.h"
#include "hardware_config.h"
#include "../control_data/robot_data.h"
#include "../control_data/control_config.h"
#include "../common/one_shot_timer.h"
#include "../control_data/csv_format.h"

class Controller {
  public:
  Controller();
  static void LaunchThread(void* arg);
  private:
  void Thread();
  void Init();
  void Execute();
  ThreadClock clock_;
  uint64_t counter_;
  // Unique
  void ReactReceivedMsg();
  void CheckTimeOut();
  void ReactControlOn(int arm_id, const ControlConfig& config);
  void ReactControlOff(int arm_id);
  void ReactCalibration(int arm_id, int joint_index);
  void ReactAppendCsv(uint32_t index, const CsvFormat csv);
  void ReactResetCsv();
  std::array<ArmController, ArmIndex::NUM_ARM> arms_;
  OneShotTimer control_off_timer_;
};

#endif // CONTROLLER_H