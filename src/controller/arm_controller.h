#ifndef ARM_CONTROLLER_H
#define AMR_CONTROLLER_H

#include "numeric_typedefs.h"
#include "hardware_config.h"
#include "differentiator.h"
#include "../control_data/arm_data.h"
#include "../control_data/control_config.h"
#include <memory>
#include "motion_generator.h"
#include "../common/ring_buffer.h"

class ArmController {
  public:
  ArmController();
  void Init(const ArmConfig& config);
  void Config(const ControlConfig& config);
  void Start();
  void Stop();
  void Update(double time);
  void CalibrateOffset(uint8_t joint_index, float q_state);
  const ArmData& RefArmData();
  void AppendCsvData(const std::array<float, PoseIndex::NUM_POSE>& pose);
  void ResetCsvData();
  private:
  void CvtInputToState(const InputData& input, StateData& state);
  void CvtRefToOutput(const RefData& ref, OutputData& output);
  void JointControl(const JointData& state, JointData& ref);
  void CvtEffortToAct(const VectorXf& effort, VectorXf& act_effort);
  ArmConfig config_;
  ControlConfig ctrl_config_;
  VectorXf q_error_integral_;
  Differentiator<VectorXf> qd_ref_filter_;
  Differentiator<VectorXf> qd_state_filter_;
  Differentiator<VectorXf> qdd_ref_filter_;
  Differentiator<VectorXf> qdd_state_filter_;
  ArmData pre_data_;
  RingBuffer<MsData> ms_data_buffer_;

  std::unique_ptr<GeneratorBase> motion_;
  uint64_t control_counter_;
};

#endif // ARM_CONTROLLER_H