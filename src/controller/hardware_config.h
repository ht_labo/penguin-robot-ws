#ifndef HARDWARE_CONFIG_H
#define HARDWARE_CONFIG_H

#include <array>
#include <stdint.h>
#include "../constants/robot_const.h"

/**
 * @brief encoder config convert voltage to pos
 */
struct EncoderConfig {
  std::array<unsigned int, NUM_JOINT> resolution; // ppr (pulse per rotate)
  std::array<float, NUM_JOINT> offset;            // rad (encoder angle with joint zero)
  std::array<float, NUM_JOINT> encoder_to_joint;  // -
};

/**
 * @brief actuator config convert pos to binary
 */
struct ActuatorConfig {
  std::array<float, NUM_JOINT> gear_ratio;     // -
  std::array<uint16_t, NUM_JOINT> offset;      // bit (actuator binary with joint zero)
  std::array<float, NUM_JOINT> torque_constant;// mNm/A
  std::array<float, NUM_JOINT> current_to_binary;  // /A
};

struct JointConfig {
  std::array<float, NUM_JOINT> q_min;
  std::array<float, NUM_JOINT> q_max;
  std::array<std::array<float, NUM_JOINT>, NUM_JOINT> parallel_ratio; // convert joint q to target q
};

struct ArmConfig {
  ArmIndex arm_id;
  EncoderConfig encoder;
  ActuatorConfig actuator;
};

struct HardwareConfig {
  ArmConfig right;
  ArmConfig left;
};

inline void GetHardwareConfig(HardwareConfig& config) {
  /* right */
  ArmConfig right;
  right.arm_id = ArmIndex::RIGHT;
  /* encoder */
  right.encoder.resolution = {{1, 4095, 4095}};
  right.encoder.offset = {{0.0f, 0.0f, 0.0f}};
  right.encoder.encoder_to_joint = {{1.0f, - 1.0f / 13.44f, 1.0f / 13.44f}};
  /* actuator */
  right.actuator.gear_ratio = {{1.0f, 13.44f, 13.44f}};
  right.actuator.offset = {{0, 2047, 2047}};
  right.actuator.torque_constant = {{1.0f, 109.5, 109.5}};
  right.actuator.current_to_binary = {{1.0f, - 4095 / 30.0, 4095 / 30.0}};
  /* left */
  ArmConfig left;
  left = right;
  left.arm_id = ArmIndex::LEFT;
  /* encoder */
  left.encoder.encoder_to_joint = {{1.0f, 1.0f / 13.44f, - 1.0f / 13.44f}};
  /* actuator */
  left.actuator.current_to_binary = {{1.0f, 4095 / 30.0, - 4095 / 30.0}};
  /* config */
  config.right = right;
  config.left = left;
}

#endif // HARDWARE_CONFIG_H