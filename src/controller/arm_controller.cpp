#include "arm_controller.h"
#include "eigen_conversions.h"
#include "math_util.h"
#include "../control_data/extern_definition.h"
#include "../kinematics/penguin_kinematics.h"

ArmController::ArmController() :
  q_error_integral_(VectorXf::Zero(NUM_JOINT)),
  qd_ref_filter_(CONTROLLER_CYCLE_TIME_MS * MS_TO_S, VectorXf::Zero(NUM_JOINT)),
  qd_state_filter_(CONTROLLER_CYCLE_TIME_MS * MS_TO_S, VectorXf::Zero(NUM_JOINT)),
  qdd_ref_filter_(CONTROLLER_CYCLE_TIME_MS * MS_TO_S, VectorXf::Zero(NUM_JOINT)),
  qdd_state_filter_(CONTROLLER_CYCLE_TIME_MS * MS_TO_S, VectorXf::Zero(NUM_JOINT)),
  motion_(new GeneratorBase()),
  control_counter_(0) {
}

void ArmController::Init(const ArmConfig& config) {
  config_ = config;
}

void ArmController::Config(const ControlConfig& config) {
  ctrl_config_ = config;
  // update reference
  if (config.control_mode == ControlMode::FK) {
    motion_.reset(new FKGenerator(NUM_JOINT));
  } else if (config.control_mode == ControlMode::IK) {
    motion_.reset(new IKGenerator(PoseIndex::NUM_POSE));
  } else if (config.control_mode == ControlMode::EFFORT) {
    motion_.reset(new EffortGenerator(NUM_JOINT));
  } else if (config.control_mode == ControlMode::ACT_EFFORT) {
    motion_.reset(new ActEffortGenerator(NUM_JOINT));
  } else if (config.control_mode == ControlMode::OPEN_LOOP) {
    motion_.reset(new OpenloopGenerator(NUM_JOINT));
  } else if ((config.control_mode == ControlMode::MS) || (config.control_mode == ControlMode::CSV)) {
    motion_.reset(new MSGenerator(NUM_JOINT));
  }
  motion_->Config(config.reference);
  motion_->CompleteConfig();  
  // reset integral
  q_error_integral_.setZero();
  // update other gain
  qd_state_filter_.SetCutoffFrequency(config.gain.other.qd_fc);
  qd_ref_filter_.SetCutoffFrequency(config.gain.other.qd_fc);
  qdd_state_filter_.SetCutoffFrequency(config.gain.other.qdd_fc);
  qdd_ref_filter_.SetCutoffFrequency(config.gain.other.qdd_fc);
  // init index
  ms_data_buffer_.SetIndex(0);
}

void ArmController::Start() {
  // reset counter
  control_counter_ = 0;
  motion_->StartRequest();
}

void ArmController::Stop() {
  motion_->StopRequest();
}

void ArmController::Update(double time) {
  InputData input;
  OutputData output(config_.actuator.offset);
  StateData state;
  RefData ref;
  MsData ms_data;

  double control_time = control_counter_ *  CONTROLLER_CYCLE_TIME_MS * MS_TO_S;
  /* Update Input */
  input_memory_[config_.arm_id].Read(input, 10 / portTICK_RATE_MS);

  /* Update State */
  CvtInputToState(input, state);

  /* Update MsData */
  if ((ctrl_config_.control_mode == ControlMode::CSV) && (ms_data_buffer_.Length() > 0)) {
    ms_data = ms_data_buffer_.PeekNext();
  }

  /* Update Motion */
  if ((ctrl_config_.control_mode == ControlMode::FK) ||
      (ctrl_config_.control_mode == ControlMode::IK) ||
      (ctrl_config_.control_mode == ControlMode::MS) ||
      (ctrl_config_.control_mode == ControlMode::CSV)) {

    /* Joint or Position Control */
    motion_->Update(control_time, state.joint, pre_data_.ref.joint, ms_data, ref.joint, output); // >> q
    // update effort ref
    JointControl(state.joint, ref.joint); // act effort
    // update act effort ref
    CvtEffortToAct(ref.joint.effort, ref.joint.act_effort); // >> act effort
    // update output binary
    CvtRefToOutput(ref, output); // >> binary
  } else if (ctrl_config_.control_mode == ControlMode::EFFORT) {

    /* Effort Control */
    motion_->Update(control_time, state.joint, pre_data_.ref.joint, ms_data, ref.joint, output); // >> effort
    // update act effort ref
    CvtEffortToAct(ref.joint.effort, ref.joint.act_effort); // >> act effort
    // update output binary
    CvtRefToOutput(ref, output); // >> binary
  } else if (ctrl_config_.control_mode == ControlMode::ACT_EFFORT) {

    /* Act Effort Control */
    // update act effort ref
    motion_->Update(control_time, state.joint, pre_data_.ref.joint, ms_data, ref.joint, output); // >> act effort
    // update output binary
    CvtRefToOutput(ref, output); // >> binary
  } else if (ctrl_config_.control_mode == ControlMode::OPEN_LOOP) {

    /* Open Loop */
    motion_->Update(control_time, state.joint, pre_data_.ref.joint, ms_data, ref.joint, output); // >> binary
  }

  /* Update Enable */
  for (size_t i = 0; i < output.motor_enable.size(); i++) {
    output.motor_enable[i] = ctrl_config_.enable[i];
  }

  /* Update Output */
  output_memory_[config_.arm_id].Write(output, 10 / portTICK_RATE_MS);

  /* Monitor */
  state.pose_q = PenguinKinematics::PoseFromAngle(state.joint.q) * RAD_TO_DEG;
  state.pose_qd = PenguinKinematics::PoseFromAngle(state.joint.qd) * RAD_TO_DEG;
  ref.pose_q = PenguinKinematics::PoseFromAngle(ref.joint.q) * RAD_TO_DEG;
  ref.pose_qd = PenguinKinematics::PoseFromAngle(ref.joint.qd) * RAD_TO_DEG;

  /* Update variable */
  pre_data_.input = input;
  pre_data_.output = output;
  pre_data_.state = state;
  pre_data_.ref = ref;
  control_counter_++;
}

const ArmData& ArmController::RefArmData() {
  return pre_data_;
}

void ArmController::CvtInputToState(const InputData& input, StateData& state) {
  // encoder
  for (size_t i = 0; i < input.encoder_binary.size(); i++) {
    state.joint.act_q[i] = 2 * PI * static_cast<float>(input.encoder_binary[i]) / config_.encoder.resolution[i] - config_.encoder.offset[i];
    state.joint.q[i] = state.joint.act_q[i] * config_.encoder.encoder_to_joint[i];
  }
  state.joint.qd = qd_state_filter_.Apply(state.joint.q);
  state.joint.qdd = qdd_state_filter_.Apply(state.joint.qd);
  // motor
  for (size_t i = 0; i < input.motor_binary.size(); i++) {
    state.joint.act_effort[i] = config_.actuator.torque_constant[i] * (input.motor_binary[i] - config_.actuator.offset[i]) / config_.actuator.current_to_binary[i];
    state.joint.effort[i] = state.joint.act_effort[i] * config_.actuator.gear_ratio[i];
  }
}

void ArmController::JointControl(const JointData& state, JointData& ref) {
  // update qd
  ref.qd = qd_ref_filter_.Apply(ref.q);
  ref.qdd = qdd_ref_filter_.Apply(ref.qd);
  // update integral
  q_error_integral_ += (ref.q - state.q);
  // update effort
  for (int i = 0; i < ref.effort.size(); i++) {
    JointGainConfig& gain = ctrl_config_.gain.joints[i];
    ref.effort_ff[i] = gain.colomb * tanhf(ref.qd[i] * SecureInv(gain.v_limit)) + gain.viscosity * ref.qd[i] + gain.inertia * ref.qdd[i];
    float effort_fb = gain.kp * ((ref.q[i] - state.q[i]) + gain.td * (ref.qd[i] - state.qd[i])) + gain.ki * q_error_integral_[i];
    ref.effort[i] = ref.effort_ff[i] + effort_fb;
  }
}

void ArmController::CvtEffortToAct(const VectorXf& effort, VectorXf& act_effort) {
  for (int i = 0 ; i < act_effort.size(); i++) {
    act_effort[i] = effort[i] / config_.actuator.gear_ratio[i];
  }
}

void ArmController::CvtRefToOutput(const RefData& ref, OutputData& output) {
  // motor
  for (size_t i = 0; i < output.motor_binary.size(); i++) {
    output.motor_binary[i] = config_.actuator.current_to_binary[i] * ref.joint.act_effort[i] / config_.actuator.torque_constant[i] + config_.actuator.offset[i];
  }
}

void ArmController::CalibrateOffset(uint8_t joint_index, float q_state) {
  float q_enc = q_state / config_.encoder.encoder_to_joint[joint_index];
  config_.encoder.offset[joint_index] = q_enc + config_.encoder.offset[joint_index];
}

void ArmController::AppendCsvData(const std::array<float, PoseIndex::NUM_POSE>& pose) {
  MsData data;
  data.active = true;
  data.pose = pose;

  ms_data_buffer_.Push(data);
}

void ArmController::ResetCsvData() {
  ms_data_buffer_.Reset();
}