#include <Arduino.h>
#include "controller/controller.h"
#include "io_module/io_module.h"
#include "mainte_com/mainte_com.h"

Controller controller_;
Thread controller_thread_;
IoModule io_module_;
Thread io_module_thread_;
MainteCom mainte_com_;
Thread mainte_com_thread_;

/**
 * @brief once run function
 */
void setup() {
  Serial.begin(921600);
  Serial.println("Start Program");
  // start mainte communication
  controller_thread_.Start(Controller::LaunchThread, "controller_thread", 5, 8192, &controller_);
  io_module_thread_.Start(IoModule::LaunchThread, "io_module_thread", 2, 8192, &io_module_);
  mainte_com_thread_.Start(MainteCom::LaunchThread, "mainte_com_thread", 10, 8192, &mainte_com_);
  // start FreeRTOS
  vTaskStartScheduler();
  Serial.println("Insufficient RAM");
}

/**
 * @brief repeatedly run function
 */
void loop() {
  // do nothing
}