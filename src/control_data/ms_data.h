#ifndef MS_DATA_H
#define MS_DATA_H

#include <array>
#include "math_constant.h"
#include "../constants/robot_const.h"

struct MsData {
  bool active;
  std::array<float, PoseIndex::NUM_POSE> pose;
  MsData():
    active(false),
    pose{0.0f, 0.0f, 0.0f} {}
};

#endif // MS_DATA_H