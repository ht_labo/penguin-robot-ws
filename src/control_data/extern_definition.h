#ifndef EXTERN_DEFINITION_H
#define EXTERN_DEFINITION_H

#include "../ipc/memory.hpp"
#include "../ipc/queue.hpp"
#include "../ipc/semaphore.hpp"

#include "robot_data.h"

/**
 * @brief extern memory defined at io_module
 */
extern std::array<ShareMemory<InputData>, ArmIndex::NUM_ARM> input_memory_;
extern std::array<ShareMemory<OutputData>, ArmIndex::NUM_ARM> output_memory_;

/**
 * @brief extern queue defined at mainte_com
 */
extern Queue mainte_to_controller_queue_;
extern Queue controller_to_mainte_queue_;
extern ShareMemory<RobotData> robot_data_memory_;

/**
 * @brief extern semaphore defined at io_module
 */
extern Semaphore sync_semaphore_;

#endif