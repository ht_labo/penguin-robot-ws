#ifndef GAIN_CONFIG_H
#define GAIN_CONFIG_H

#include "../constants/robot_const.h"
#include "math_constant.h"
#include <array>

#pragma pack(push, 1)
struct JointGainConfig {
  float kp;
  float td;
  float ki;
  float colomb;
  float v_limit;
  float viscosity;
  float inertia;
  JointGainConfig():
    kp(0.0f),
    td(0.0f),
    ki(0.0f),
    colomb(0.0f),
    v_limit(0.0f),
    viscosity(0.0f),
    inertia(0.0f) {}
};

struct OtherGainConfig {
  float qd_fc;
  float qdd_fc;
  OtherGainConfig():
    qd_fc(10.0f),
    qdd_fc(10.0f) {}
};

struct GainConfig {
  std::array<JointGainConfig, NUM_JOINT> joints;
  OtherGainConfig other;
};

#pragma pack(pop)

inline void GetGainConfig(GainConfig& config) {
  // joint gain config
  for (int i = 0; i < config.joints.size(); i++) {
    config.joints[i].kp = 2000.0f;
    config.joints[i].td = 0.25f;
    config.joints[i].ki = 0.0f;
  }
  // other gain config
  config.other.qd_fc = 10.0f;
  config.other.qdd_fc = 10.0f;
}

#endif // GAIN_CONFIG_H