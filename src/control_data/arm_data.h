#ifndef VARIABLE_DATA_H
#define VARIABLE_DATA_H

#include "../constants/robot_const.h"
#include "math_constant.h"
#include "numeric_typedefs.h"
#include "io_data.h"

struct JointData {
  VectorXf q;           // joint pos [rad or m]
  VectorXf qd;          // joint vel [rad/s or m/s]
  VectorXf qdd;         // joint acc [rad/s^2 or m/s^2]
  VectorXf effort;      // joint torque [Nm or N]
  VectorXf effort_ff;   // joint torque [Nm or N]
  VectorXf act_q;       // actuator pos [rad or m]
  VectorXf act_qd;      // actuator vel [rad or m]
  VectorXf act_effort;  // actuator torque [Nm or N]
  explicit JointData(uint8_t num_of_joints = 0) {
    q = VectorXf::Zero(num_of_joints);
    qd = VectorXf::Zero(num_of_joints);
    qdd = VectorXf::Zero(num_of_joints);
    effort = VectorXf::Zero(num_of_joints);
    effort_ff = VectorXf::Zero(num_of_joints);
    act_q = VectorXf::Zero(num_of_joints);
    act_qd = VectorXf::Zero(num_of_joints);
    act_effort = VectorXf::Zero(num_of_joints);
  }
};

struct StateData {
  JointData joint;
  VectorXf pose_q;       // pos [rad]
  VectorXf pose_qd;      // vel [rad/s]
  VectorXf pose_effort;  // torque [Nm]
  StateData():
    joint(NUM_JOINT),
    pose_q(PoseIndex::NUM_POSE),
    pose_qd(PoseIndex::NUM_POSE),
    pose_effort(PoseIndex::NUM_POSE) {}
};

struct RefData {
  JointData joint;
  VectorXf pose_q;       // pos [rad]
  VectorXf pose_qd;      // vel [rad/s]
  VectorXf pose_effort;  // torque [Nm]
  RefData():
    joint(NUM_JOINT),
    pose_q(PoseIndex::NUM_POSE),
    pose_qd(PoseIndex::NUM_POSE),
    pose_effort(PoseIndex::NUM_POSE) {}
};

struct ArmData {
  InputData   input;
  OutputData  output;
  StateData   state;
  RefData     ref;
};

#endif // VARIABLE_DATA_H