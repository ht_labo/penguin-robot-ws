#ifndef CONTROL_CONFIG_H
#define CONTROL_CONFIG_H

#include <array>
#include "wave.h"
#include "../constants/robot_const.h"
#include "math_constant.h"
#include "gain_config.h"
#include <stdint.h>

#pragma pack(push, 1)
struct Reference {
  std::array<WaveForm, NUM_JOINT> fk;
  std::array<WaveForm, PoseIndex::NUM_POSE> ik;
  std::array<WaveForm, NUM_JOINT> effort;
  std::array<WaveForm, NUM_JOINT> act_effort;
  std::array<WaveForm, NUM_JOINT> openloop;
};

struct ControlConfig {
  uint8_t control_mode;
  Reference reference;
  GainConfig gain;
  std::array<uint8_t, NUM_JOINT> enable;
};

#pragma pack(pop)

#endif // CONTROL_CONFIG_H