#ifndef CSV_FORMAT_H
#define CSV_FORMAT_H

#include <array>
#include "../constants/robot_const.h"

#pragma pack(push, 1)
struct CsvFormat {
  std::array<float, NUM_POSE> right;
  std::array<float, NUM_POSE> left;   
};

#pragma pack(pop)

#endif // CSV_FORMAT_H