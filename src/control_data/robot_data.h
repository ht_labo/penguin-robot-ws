#ifndef ROBOT_DATA_H
#define ROBOT_DATA_H

#include "arm_data.h"

struct RobotData {
  ArmData right;
  ArmData left;
};

#endif // ROBOT_DATA_H