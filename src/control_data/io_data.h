#ifndef IO_DATA_H
#define IO_DATA_H

#include <array>
#include <stdint.h>
#include "../constants/robot_const.h"

struct InputData {
  uint64_t count;
  std::array<uint64_t, 5> lap_time;
  std::array<int32_t, NUM_JOINT> encoder_binary;
  std::array<int32_t, NUM_JOINT> motor_binary;
  InputData():
    count(0),
    lap_time{0, 0, 0, 0, 0} {}
};

struct OutputData {
  std::array<uint16_t, NUM_JOINT> motor_binary;
  std::array<int8_t, NUM_JOINT> motor_enable;
  OutputData() {}
  OutputData(std::array<uint16_t, NUM_JOINT>& motor_binary) :
    motor_binary(motor_binary) {}
};

#endif // IO_DATA_H