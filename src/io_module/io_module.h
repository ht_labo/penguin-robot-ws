#ifndef IO_MODULE_H
#define IO_MODULE_H
#include "../ipc/thread.hpp"
#include "../control_data/io_data.h"
#include "io_config.h"
#include <AS5600.h>
#include <MCP_DAC.h>

class IoModule {
  public:
  IoModule();
  static void LaunchThread(void* arg);
  private:
  void Thread();
  void Init();
  void Execute();
  ThreadClock clock_;
  uint64_t counter_;

  std::array<InputData, ArmIndex::NUM_ARM> inputs_;
  std::array<OutputData, ArmIndex::NUM_ARM> outputs_;
  std::array<TwoWire, NUM_AS5600> wire_;
  std::array<AS5600, NUM_AS5600> as5600_;
  std::array<SPIClass, NUM_MCP4922> spi_;
  std::array<uint32_t, NUM_MCP4922> spi_cs_;
  std::array<MCP4922, NUM_MCP4922> mcp4922_;
  std::array<uint32_t, NUM_ANALOG_IN> analog_in_pin_;
  std::array<uint32_t, NUM_DIGITAL_OUT> digital_out_pin_;

  std::array<int32_t, NUM_AS5600_ALL_CH> turn_count_;
  std::array<int32_t, NUM_AS5600_ALL_CH> pre_encoder_binary_;
};

#endif // IO_MODULE_H