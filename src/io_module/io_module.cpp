#include "io_module.h"
#include "../control_data/extern_definition.h"

/**
 * @brief Global parameter
 */
std::array<ShareMemory<InputData>, ArmIndex::NUM_ARM> input_memory_;
std::array<ShareMemory<OutputData>, ArmIndex::NUM_ARM> output_memory_;
Semaphore sync_semaphore_;

/**
 * @class IoModule
 */
IoModule::IoModule() :
  clock_(IO_MODULE_CYCLE_TIME_MS),
  counter_(0),
  wire_{TwoWire(I2C1_SDA_PIN, I2C1_SCL_PIN), TwoWire(I2C2_SDA_PIN, I2C2_SCL_PIN), TwoWire(I2C3_SDA_PIN, I2C3_SCL_PIN), TwoWire(I2C4_SDA_PIN, I2C4_SCL_PIN)}, // SDA,SCL
  as5600_{AS5600(&wire_[0]), AS5600(&wire_[1]), AS5600(&wire_[2]), AS5600(&wire_[3])},
  spi_{SPIClass(SPI4_MOSI_PIN, SPI4_MISO_PIN, SPI4_SCL_PIN), SPIClass(SPI5_MOSI_PIN, SPI5_MISO_PIN, SPI5_SCL_PIN)},// SCK, MISO, MOSI
  spi_cs_{SPI4_CS_PIN, SPI5_CS_PIN},
  mcp4922_{MCP4922(255, 255, &spi_[0]), MCP4922(255, 255, &spi_[1])},
  analog_in_pin_{ANALOG_IN0_PIN, ANALOG_IN1_PIN, ANALOG_IN2_PIN, ANALOG_IN3_PIN},
  digital_out_pin_{DIGITAL_OUT0_PIN, DIGITAL_OUT1_PIN, DIGITAL_OUT2_PIN, DIGITAL_OUT3_PIN},
  turn_count_{0, 0, 0, 0} {}

void IoModule::Init() {
  Serial.println("IoModule : Init");
  // Init I2C & AS5600
  #ifndef SIMULATION_MODE
  for (size_t i = 0; i < as5600_.size(); i++) {
    wire_[i].begin();
    wire_[i].setClock(400000);
    wire_[i].setTimeout(500 /* us */);
    as5600_[i].begin();
    pre_encoder_binary_[i] = static_cast<int32_t>(as5600_[i].readAngle());
  }
  #endif
  // Init SPI & MCP4922
  for (size_t i = 0; i < mcp4922_.size(); i++) {
    mcp4922_[i].begin(spi_cs_[i]);
  }
  // Init DigitalWrite
  for (size_t i = 0; i < digital_out_pin_.size(); i++) {
    pinMode(digital_out_pin_[i], OUTPUT);
    digitalWrite(digital_out_pin_[i], LOW);
  }
  // Init AnalogRead
  analogReadResolution(12);
}

void IoModule::Execute() {
  /* Output */
  /* read from shared memory */
  for (size_t i = 0; i < output_memory_.size(); i++) {
    output_memory_[i].Read(outputs_[i], 10 / portTICK_RATE_MS);
  }

  /* MCP4922 */
  for (size_t i = 0; i < MCP4922_TABLE.size(); i++) {
    IoTable& table = MCP4922_TABLE[i];
    if (table.type == IoType::MOTOR) {
      // update motor (escon output)
      mcp4922_[table.device_index].analogWrite(outputs_[table.arm].motor_binary[table.index], table.device_ch);
    }
  }

  /* DigitalOutput */
  for (size_t i = 0; i < DIGITAI_OUT_TABLE.size(); i++) {
    IoTable& table = DIGITAI_OUT_TABLE[i];
    if (table.type == IoType::DO) {
      digitalWrite(digital_out_pin_[table.device_ch], outputs_[table.arm].motor_enable[table.index]);
    }
  }

  /* Input */
  /* AS5600 */
  for (size_t i = 0; i < AS5600_TABLE.size(); i++) {
    IoTable& table = AS5600_TABLE[i];
    if (table.type == IoType::ENCODER) {
      // update encoder
      #ifndef SIMULATION_MODE
      inputs_[table.arm].encoder_binary[table.index] = static_cast<int32_t>(as5600_[table.device_index].readAngle());
      #endif
      // single -> mult turn
      if (inputs_[table.arm].encoder_binary[table.index] < pre_encoder_binary_[i] - 2047) {
        turn_count_[i]++;
      } else if (inputs_[table.arm].encoder_binary[table.index] > pre_encoder_binary_[i] + 2047) {
        turn_count_[i]--;
      }
      pre_encoder_binary_[i] = inputs_[table.arm].encoder_binary[table.index];
      inputs_[table.arm].encoder_binary[table.index] += 4095 * turn_count_[i];
    }
  }

  /* AnalogInput */
  for (size_t i = 0; i < ANALOG_IN_TABLE.size(); i++) {
    IoTable& table = ANALOG_IN_TABLE[i];
    if (table.type == IoType::MOTOR) {
      // update motor (escon output)
      inputs_[table.arm].motor_binary[table.index] = analogRead(analog_in_pin_[table.device_ch]);
    }
  }

  /* write to shared memory */
  for (size_t i = 0; i < input_memory_.size(); i++) {
    inputs_[i].count = counter_;
    input_memory_[i].Write(inputs_[i], 10 / portTICK_RATE_MS);
  }

  /* Semaphore */
  if (counter_ % static_cast<int>(CONTROLLER_CYCLE_TIME_MS / IO_MODULE_CYCLE_TIME_MS) == 0) {
    sync_semaphore_.Give();
  }
}

void IoModule::Thread() {
  Init();
  while(true) {
    // cycle period
    clock_.Wait();
    // main process
    Execute();
    // update
    counter_++;
  }
}

void IoModule::LaunchThread(void* arg) {
  // start thread
  reinterpret_cast<IoModule*>(arg)->Thread();
}