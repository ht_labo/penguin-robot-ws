#ifndef IO_CONFIG_H
#define IO_CONFIG_H

#include "../constants/robot_const.h"
#include <array>
#include <stdint.h>
#include <Arduino.h>

// #define SIMULATION_MODE

/* num define */
#define NUM_DIGITAL_OUT 4
#define NUM_ANALOG_IN 4
#define NUM_AS5600 4
#define NUM_AS5600_CH 1
#define NUM_AS5600_ALL_CH (NUM_AS5600 * NUM_AS5600_CH)
#define NUM_MCP4922 2
#define NUM_MCP4922_CH 2
#define NUM_MCP4922_ALL_CH (NUM_MCP4922 * NUM_MCP4922_CH)

/**
 * @brief pin define
 * @note see https://os.mbed.com/platforms/ST-Nucleo-F767ZI/
 */
// I2C
#define I2C1_SDA_PIN PB9
#define I2C1_SCL_PIN PB8
#define I2C2_SDA_PIN PB11
#define I2C2_SCL_PIN PB10
#define I2C3_SDA_PIN PC9
#define I2C3_SCL_PIN PA8
#define I2C4_SDA_PIN PD13
#define I2C4_SCL_PIN PD12
// SPI
#define SPI4_SCL_PIN  PE2
#define SPI4_MISO_PIN PE13 // unused
#define SPI4_MOSI_PIN PE6
#define SPI4_CS_PIN   PD3
#define SPI5_SCL_PIN  PF7
#define SPI5_MISO_PIN PF8 // unused
#define SPI5_MOSI_PIN PF9
#define SPI5_CS_PIN   PE3
// Analog In
#define ANALOG_IN0_PIN PA2
#define ANALOG_IN1_PIN PA0
#define ANALOG_IN2_PIN PC0
#define ANALOG_IN3_PIN PF10
// Digital Out
#define DIGITAL_OUT0_PIN PA10
#define DIGITAL_OUT1_PIN PD11
#define DIGITAL_OUT2_PIN PC1
#define DIGITAL_OUT3_PIN PE5
// Ethernet
#define ETHERNET_CLOCK      PA1 // unused
#define ETHERNET_MDIO       PA2  // unused
#define ETHERNTT_MDC        PC1 // unused
#define ETHERNET_RX_VAILD   PA7 // unused
#define ETHERNET_RX0        PC4 // unused
#define ETHERNET_RX1        PC5 // unused
#define ETHERNET_TX_ENABLE  PG11 // unused
#define ETHERNET_TX0        PG13 // unused
#define ETHERNET_TX1        PB13 // unused
// Serial(USART3)
#define USB_SERIAL_TX       PD8
#define USB_SERIAL_RX       PD9

enum IoType {
  DI,
  DO,
  AI,
  AO,
  ENCODER,
  MOTOR,
};

enum IoIndex {
  IO_0,
  IO_1,
  IO_2,
  IO_3,
  IO_4
};

struct IoTable {
  uint8_t device_index;
  uint8_t device_ch;
  ArmIndex arm;
  IoType type;
  IoIndex index;
};

static std::array<IoTable, NUM_AS5600_ALL_CH> AS5600_TABLE = {{
  {0, 0, ArmIndex::RIGHT, IoType::ENCODER, IoIndex::IO_1},
  {1, 0, ArmIndex::RIGHT, IoType::ENCODER, IoIndex::IO_2},
  {2, 0, ArmIndex::LEFT,  IoType::ENCODER, IoIndex::IO_1},
  {3, 0, ArmIndex::LEFT,  IoType::ENCODER, IoIndex::IO_2}
}};

static std::array<IoTable, NUM_MCP4922_ALL_CH> MCP4922_TABLE = {{
  {0, 0, ArmIndex::RIGHT, IoType::MOTOR, IoIndex::IO_1},
  {0, 1, ArmIndex::RIGHT, IoType::MOTOR, IoIndex::IO_2},
  {1, 0, ArmIndex::LEFT,  IoType::MOTOR, IoIndex::IO_1},
  {1, 1, ArmIndex::LEFT,  IoType::MOTOR, IoIndex::IO_2}
}};

static std::array<IoTable, NUM_ANALOG_IN> ANALOG_IN_TABLE = {{
  {0, 0, ArmIndex::RIGHT,  IoType::MOTOR, IoIndex::IO_1},
  {0, 1, ArmIndex::RIGHT,  IoType::MOTOR, IoIndex::IO_2},
  {0, 2, ArmIndex::LEFT,   IoType::MOTOR, IoIndex::IO_1},
  {0, 3, ArmIndex::LEFT,   IoType::MOTOR, IoIndex::IO_2}
}};

static std::array<IoTable, NUM_DIGITAL_OUT> DIGITAI_OUT_TABLE = {{
  {0, 0, ArmIndex::RIGHT,  IoType::DO, IoIndex::IO_1},
  {0, 1, ArmIndex::RIGHT,  IoType::DO, IoIndex::IO_2},
  {0, 2, ArmIndex::LEFT,   IoType::DO, IoIndex::IO_1},
  {0, 3, ArmIndex::LEFT,   IoType::DO, IoIndex::IO_2}
}};

#endif // IO_CONFIG_H