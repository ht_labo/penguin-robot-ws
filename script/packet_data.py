#!/usr/bin/env python3
# coding: utf-8

from ctypes import *
from control_config import *
from csv_format import *

class PacketType(IntEnum):
    PACKET_UNKNOWN = 0
    # mainte to robot
    PACKET_SERVER_TO_ROBOT_CONTROL_ON = auto()
    PACKET_SERVER_TO_ROBOT_CONTROL_OFF = auto()
    PACKET_SERVER_TO_ROBOT_CALIBRATION = auto()
    PACKET_SERVER_TO_ROBOT_ALL_CONTROL_ON = auto()
    PACKET_SERVER_TO_ROBOT_ALL_CONTROL_OFF = auto()
    PACKET_SERVER_TO_ROBOT_CSV_APPEND_REQ = auto()
    PACKET_SERVER_TO_ROBOT_CSV_RESET = auto()
    # robot to mainte
    PACKET_ROBOT_TO_SERVER_ROBOT_INFO = auto()
    PACKET_ROBOT_TO_SERVER_CSV_APPEND_RES = auto()
    # terminate
    PACKET_TERMINATE = auto()
class TcpHeader(Structure):
    _pack_ = 1
    _fields_ = [
        ('size', c_uint32),
        ('type', c_uint8)
    ]
    def __init__(self, size = 0, type = 0):
        self.size = size
        self.type = type
class Packet(Structure):
    _pack_ = 1
    _fields_ = [
        ('header', TcpHeader)
    ]
    def __init__(self, type = 0):
        self.header = TcpHeader(sizeof(Packet), type)
class PacketControl(Structure):
    _pack_ = 1
    _fields_ = [
        ('header', TcpHeader),
        ('arm_id', c_uint8),
        ('config', ControlConfig)
    ]
    def __init__(self, type = 0, arm_id = 0, config = ControlConfig()):
        self.header = TcpHeader(sizeof(PacketControl), type)
        self.arm_id = arm_id
        self.config = config
class PacketCalibration(Structure):
    _pack_ = 1
    _fields_ = [
        ('header', TcpHeader),
        ('arm_id', c_uint8),
        ('joint_index', c_uint8)
    ]
    def __init__(self, arm_id = 0, joint_index = 0):
        self.header = TcpHeader(sizeof(PacketControl), PacketType.PACKET_SERVER_TO_ROBOT_CALIBRATION)
        self.arm_id = arm_id
        self.joint_index = joint_index
class PacketRobotInfo(Structure):
    _pack_ = 1
    _fields_ = [
        ('header', TcpHeader),
        ('num_arm', c_uint8),
        ('num_joint', c_uint8)
    ]
    def __init__(self):
        self.header = TcpHeader(sizeof(PacketRobotInfo), PacketType.PACKET_ROBOT_TO_SERVER_ROBOT_INFO)
        self.num_arm = 0
        self.num_joint = 0
class PacketAllControl(Structure):
    _pack_ = 1
    _fields_ = [
        ('header', TcpHeader),
        ('operation_time', c_uint32),
        ('config', ControlConfig * NUM_ARM)
    ]
    def __init__(self, type, time, config):
        self.header = TcpHeader(sizeof(PacketControl), type)
        self.operation_time = time
        self.config = config
class PacketCsvAppendReq(Structure):
    _pack_ = 1
    _fields_ = [
        ('header', TcpHeader),
        ('index', c_uint32),
        ('data', CsvFormat)
    ]
    def __init__(self, index, data):
        self.header = TcpHeader(sizeof(PacketCsvAppendReq), PacketType.PACKET_SERVER_TO_ROBOT_CSV_APPEND_REQ)
        self.index = index
        self.data = data
class PacketCsvAppendRes(Structure):
    _pack_ = 1
    _fields_ = [
        ('header', TcpHeader),
        ('result', c_bool)
    ]
    def __init__(self, result = 0):
        self.header = TcpHeader(sizeof(PacketCsvAppendRes), PacketType.PACKET_ROBOT_TO_SERVER_CSV_APPEND_RES)
        self.result = result

def GetPacketType(data):
    tcp_header = TcpHeader()
    memmove(addressof(tcp_header), data, sizeof(tcp_header))
    return tcp_header.type

if __name__ == '__main__':
    packet_control = PacketControl()
    print("PacketControl Size={}, Size in header = {}", sizeof(PacketControl), packet_control.header.size)
    packet_robot_info = PacketRobotInfo()
    print("PacketRobotInfo Size={}, Size in header = {}", sizeof(packet_robot_info), packet_control.header.size)