#!/usr/bin/env python3
# coding: utf-8

from robot_const import *
from ctypes import *
import yaml

class JointGainConfig(Structure):
  _pack_ = 1
  _fields_ = [
    ('kp', c_float),
    ('td', c_float),
    ('ki', c_float),
    ('colomb', c_float),
    ('v_limit', c_float),
    ('viscosity', c_float),
    ('inertia', c_float),
  ]
class OtherGainConfig(Structure):
  _pack_ = 1
  _fields_ = [
    ('qd_fc', c_float),
    ('qdd_fc', c_float)
  ]
class GainConfig(Structure):
  _pack_ = 1
  _fields_ = [
    ('joints', JointGainConfig * NUM_JOINT),
    ('other', OtherGainConfig)
  ]

def print_gain(gain):
  # joint gain
  for i in range(len(gain.joints)):
    print('Joint' + str(i))
    for field in gain.joints[i]._fields_:
      print(field[0] + ' : ' + str(getattr(gain.joints[i], field[0])))
  # other gain
  for field in gain.other._fields_:
    print(field[0] + ' : ' + str(getattr(gain.other, field[0])))

def open_gain(yaml_file):
  ret = GainConfig() 
  with open(yaml_file, 'r') as file:
    obj = yaml.safe_load(file)
    # joint
    joint_labels = ['Joint' + str(i) for i in range(len(ret.joints))]
    for i in range(len(joint_labels)):
      for field in ret.joints[i]._fields_:
        if field[0] in obj['JOINT_GAIN'][joint_labels[i]]:
          var = obj['JOINT_GAIN'][joint_labels[i]][field[0]]
        else:
          var = 0.0
        setattr(ret.joints[i], field[0], var)
    # other
    for field in ret.other._fields_:
      if field[0] in obj['OTHER_GAIN']:
        var = obj['OTHER_GAIN'][field[0]]
      else:
        var = 0.0
      setattr(ret.other, field[0], var)
  return ret

def save_gain(yaml_file, gain):
  with open(yaml_file, 'w') as file:
    out = {}
    # joint
    joint_labels = ['Joint' + str(i) for i in range(len(gain.joints))]
    joint_gain_form = {}
    for i in range(len(joint_labels)):
      form = {}
      for field in gain.joints[i]._fields_:
        form[field[0]] = getattr(gain.joints[i], field[0])
      joint_gain_form[joint_labels[i]] = form
    out['JOINT_GAIN'] = joint_gain_form
    # other
    form = {}
    for field in gain.other._fields_:
      form[field[0]] = getattr(gain.other, field[0])
    out['OTHER_GAIN'] = form
    yaml.dump(out, file)