#!/usr/bin/env python3
# coding: utf-8

import tkinter
from tkinter import ttk
from tkinter import filedialog
from ttkthemes import ThemedTk
from gain_config import *

class GainGui(ttk.Frame):
    def __init__(self, master, arm_index, gain_config):
        super().__init__(master)
        # init parameter
        self.config = gain_config
        # init frame parameter
        self.joint_gain_list = []
        for i in range(NUM_JOINT):
            joint_gain_dic = {}
            for field in self.config.joints[0]._fields_:
                joint_gain_dic[field[0]] = tkinter.StringVar()
            self.joint_gain_list.append(joint_gain_dic)
        other_gain_dic = {}
        for field in self.config.other._fields_:
            other_gain_dic[field[0]] = tkinter.StringVar()
        self.other = other_gain_dic
        # set frame parameter
        self.gain_config_to_gui(self.config)
        # prepare frame
        master.title('Gain' + str(arm_index))
        self.create_widgets()
        self.pack()

    def create_widgets(self):
        # frame
        option_frame = ttk.Frame(self)
        gain_tab = ttk.Notebook(self)
        joint_gain_tab = ttk.Frame(gain_tab)
        other_gain_tab = ttk.Frame(gain_tab)
        menu_frame =  ttk.Frame(self)

        # create option_frame
        open_button = ttk.Button(option_frame, text='Open', command=self.open_callback, width=7)
        open_button.grid(row = 0, column = 1)
        save_button = ttk.Button(option_frame, text='Save', command=self.save_callback, width=7)
        save_button.grid(row = 0, column = 2)
        #set option_frame to main
        option_frame.pack(anchor = tkinter.E)

        # create joint_gain_tab
        # add joint_gain_list
        joint_gain_label_list = list(self.joint_gain_list[0].keys()) # kp td ki ...
        for i in range(len(joint_gain_label_list)):
            label = ttk.Label(joint_gain_tab, text=joint_gain_label_list[i])
            label.grid(row = i + 1, column = 0)
        for i in range(len(self.joint_gain_list)):
            label = ttk.Label(joint_gain_tab, text='Joint' + str(i))
            label.grid(row = 0, column = i + 1)
            for j in range(len(joint_gain_label_list)):
                spin = ttk.Spinbox(joint_gain_tab, textvariable=self.joint_gain_list[i][joint_gain_label_list[j]], from_=0.0,to=180.0,increment=0.1, width=7)
                spin.grid(row = j + 1, column = i + 1)
        # set joint_gain_tab to gain_tab
        joint_gain_tab.pack()
        gain_tab.add(joint_gain_tab, text='Joint Gain')

        # create other_gain_tab
        # add other
        other_gain_label_list = list(self.other.keys()) # qd qdd ...
        for i in range(len(other_gain_label_list)):
            label = ttk.Label(other_gain_tab, text=other_gain_label_list[i])
            label.grid(row = i + 1, column = 0)
        for j in range(len(other_gain_label_list)):
            spin = ttk.Spinbox(other_gain_tab, textvariable=self.other[other_gain_label_list[j]], from_=0.0,to=180.0,increment=0.1, width=7)
            spin.grid(row = j + 1, column = 1)
        # set other_gain_tab to gain_tab
        other_gain_tab.pack()
        gain_tab.add(other_gain_tab, text='Other Gain')

        # set gain_tab to main
        gain_tab.pack()

        # create menu_frame
        cancel_button = ttk.Button(menu_frame, text='Cancel', command=self.cancal_callback, width=7)
        ok_button = ttk.Button(menu_frame, text='OK', command=self.ok_callback, width=7)
        cancel_button.grid(row = 0, column = 0)
        ok_button.grid(row = 0, column = 1)
        #set menu_frame to main
        menu_frame.pack(anchor = tkinter.E)

    def gain_config_to_gui(self, gain_config):
        # joint gain
        for i in range(len(self.joint_gain_list)):
            for field in gain_config.joints[i]._fields_:
                self.joint_gain_list[i][field[0]].set(getattr(gain_config.joints[i], field[0]))
        # other gain
        for field in gain_config.other._fields_:
            self.other[field[0]].set(getattr(gain_config.other, field[0]))

    def gui_to_gain_config(self):
        ret = self.config
        # joint gain
        for i in range(len(ret.joints)):
            for field in ret.joints[i]._fields_:
                var = float(self.joint_gain_list[i][field[0]].get())
                setattr(ret.joints[i], field[0], var)
        # other gain
        for field in ret.other._fields_:
            var = float(self.other[field[0]].get())
            setattr(ret.other, field[0], var)
        return ret

    # Callback Function --->>>
    def open_callback(self):
        print('call open')
        type = [('Gain File','*.yaml')] 
        gain_file = filedialog.askopenfilename(filetypes = type) 
        self.gain_config_to_gui(open_gain(gain_file))

    def save_callback(self):
        print('call save')
        type = [('Gain File','*.yaml')] 
        gain_file = filedialog.asksaveasfilename(filetypes = type) 
        save_gain(gain_file, self.gui_to_gain_config())

    def ok_callback(self):
        print('Call ok')
        self.config = self.gui_to_gain_config()
        self.master.destroy()

    def cancal_callback(self):
        print('Call cancel')
        self.master.destroy()
    # <<<--- Callback Function

if __name__ == '__main__':
    config = GainConfig()
    print_gain(config)
    root = ThemedTk(theme='radiance')
    app = GainGui(master=root, arm_index=0, gain_config=config)
    app.mainloop()