#!/usr/bin/env python3
# coding: utf-8

from robot_const import *
from ctypes import *
from wave import *
import yaml

class Reference(Structure):
  _pack_ = 1
  _fields_ = [
    ('fk', WaveForm * NUM_JOINT),
    ('ik', WaveForm * NUM_POSE),
    ('effort', WaveForm * NUM_JOINT),
    ('act_effort', WaveForm * NUM_JOINT),
    ('open_loop', WaveForm * NUM_JOINT)
  ]

def open_reference(yaml_file) :
  reference = Reference()
  with open(yaml_file, 'r') as file:
    obj = yaml.safe_load(file)
    joint_labels = ['Joint' + str(i) for i in range(len(reference.fk))]
    pos_labels = ['Flap', 'Feather', 'Lead-lag']
    
    for i in range(len(joint_labels)):
      # FK
      reference.fk[i].type = WaveType[obj['FK'][joint_labels[i]]['type']]
      reference.fk[i].amp = obj['FK'][joint_labels[i]]['amp']
      reference.fk[i].base = obj['FK'][joint_labels[i]]['base']
      reference.fk[i].freq = obj['FK'][joint_labels[i]]['freq']
      reference.fk[i].phase = obj['FK'][joint_labels[i]]['phase']
      # EFFORT
      reference.effort[i].type = WaveType[obj['EFFORT'][joint_labels[i]]['type']]
      reference.effort[i].amp = obj['EFFORT'][joint_labels[i]]['amp']
      reference.effort[i].base = obj['EFFORT'][joint_labels[i]]['base']
      reference.effort[i].freq = obj['EFFORT'][joint_labels[i]]['freq']
      reference.effort[i].phase = obj['EFFORT'][joint_labels[i]]['phase']
      # ACT_EFFORT
      reference.act_effort[i].type = WaveType[obj['ACT_EFFORT'][joint_labels[i]]['type']]
      reference.act_effort[i].amp = obj['ACT_EFFORT'][joint_labels[i]]['amp']
      reference.act_effort[i].base = obj['ACT_EFFORT'][joint_labels[i]]['base']
      reference.act_effort[i].freq = obj['ACT_EFFORT'][joint_labels[i]]['freq']
      reference.act_effort[i].phase = obj['ACT_EFFORT'][joint_labels[i]]['phase']
      # OPEN_LOOP
      reference.open_loop[i].type = WaveType[obj['OPEN_LOOP'][joint_labels[i]]['type']]
      reference.open_loop[i].amp = obj['OPEN_LOOP'][joint_labels[i]]['amp']
      reference.open_loop[i].base = obj['OPEN_LOOP'][joint_labels[i]]['base']
      reference.open_loop[i].freq = obj['OPEN_LOOP'][joint_labels[i]]['freq']
      reference.open_loop[i].phase = obj['OPEN_LOOP'][joint_labels[i]]['phase']
    for i in range(len(pos_labels)):
      # IK
      reference.ik[i].type = WaveType[obj['IK'][pos_labels[i]]['type']]
      reference.ik[i].amp = obj['IK'][pos_labels[i]]['amp']
      reference.ik[i].base = obj['IK'][pos_labels[i]]['base']
      reference.ik[i].freq = obj['IK'][pos_labels[i]]['freq']
      reference.ik[i].phase = obj['IK'][pos_labels[i]]['phase']
  return reference

def save_reference(yaml_file, reference):
  with open(yaml_file, 'w') as file:
    joint_labels = ['Joint' + str(i) for i in range(len(reference.fk))]
    pos_labels = ['Flap', 'Feather', 'Lead-lag']

    out = {}
    # FK
    fk_form = {}
    for i in range(len(joint_labels)):
      form = {'type' : WaveType(reference.fk[i].type).name,
              'amp' : reference.fk[i].amp,
              'base' : reference.fk[i].base,
              'freq' : reference.fk[i].freq,
              'phase' : reference.fk[i].phase}
      fk_form[joint_labels[i]] = form
    out['FK'] = fk_form
    # IK
    ik_form = {}
    for i in range(len(pos_labels)):
      form = {'type' : WaveType(reference.ik[i].type).name,
              'amp' : reference.ik[i].amp,
              'base' : reference.ik[i].base,
              'freq' : reference.ik[i].freq,
              'phase' : reference.ik[i].phase}
      ik_form[pos_labels[i]] = form
    out['IK'] = ik_form
    # EFFORT
    effort_form = {}
    for i in range(len(joint_labels)):
      form = {'type' : WaveType(reference.effort[i].type).name,
              'amp' : reference.effort[i].amp,
              'base' : reference.effort[i].base,
              'freq' : reference.effort[i].freq,
              'phase' : reference.effort[i].phase}
      effort_form[joint_labels[i]] = form
    out['EFFORT'] = effort_form
    # ACT_EFFORT
    act_effort_form = {}
    for i in range(len(joint_labels)):
      form = {'type' : WaveType(reference.act_effort[i].type).name,
              'amp' : reference.act_effort[i].amp,
              'base' : reference.act_effort[i].base,
              'freq' : reference.act_effort[i].freq,
              'phase' : reference.act_effort[i].phase}
      act_effort_form[joint_labels[i]] = form
    out['ACT_EFFORT'] = act_effort_form
    # OPEN_LOOP
    open_loop_form = {}
    for i in range(len(joint_labels)):
      form = {'type' : WaveType(reference.open_loop[i].type).name,
              'amp' : reference.open_loop[i].amp,
              'base' : reference.open_loop[i].base,
              'freq' : reference.open_loop[i].freq,
              'phase' : reference.open_loop[i].phase}
      open_loop_form[joint_labels[i]] = form
    out['OPEN_LOOP'] = open_loop_form
    yaml.dump(out, file)

if __name__ == '__main__':
  # save
  reference = Reference()
  save_reference("reference.yaml", reference)
  ret_reference = open_reference("reference.yaml")