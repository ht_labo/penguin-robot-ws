#!/usr/bin/env python3
# coding: utf-8

import tkinter
from tkinter import ttk
from tkinter import filedialog
from ttkthemes import ThemedTk
import pandas as pd

class CsvGui(ttk.Frame):
    def __init__(self, master, call_back):
        super().__init__(master)
        # init parameter
        self.df_view = pd.DataFrame()
        self.call_back = call_back
        # prepare frame
        master.title('Csv')
        self.create_widgets()
        self.pack()

    def create_widgets(self):
        # frame
        option_frame = ttk.Frame(self)
        csv_frame = ttk.Frame(self)
        menu_frame =  ttk.Frame(self)
        # create option_frame widgets
        open_button = ttk.Button(option_frame, text='Open', command=self.open_callback, width=7)
        open_button.grid(row = 0, column = 0)
        reset_button = ttk.Button(option_frame, text='Reset', command=self.reset_callback, width=7)
        reset_button.grid(row = 0, column = 1)
        #set option_frame widgets
        option_frame.pack(anchor = tkinter.E)
        # create csv_frame widgets

        # set csv_frame widgets
        csv_frame.pack()
        # create menu_frame widgets
        cancel_button = ttk.Button(menu_frame, text='Cancel', command=self.cancal_callback, width=7)
        ok_button = ttk.Button(menu_frame, text='OK', command=self.ok_callback, width=7)
        cancel_button.grid(row = 0, column = 0)
        ok_button.grid(row = 0, column = 1)
        #set menu_frame widgets
        menu_frame.pack(anchor = tkinter.E)

    # Callback Function --->>>
    def open_callback(self):
        print('call open')
        type = [('Csv File','*.csv')] 
        file = filedialog.askopenfilename(filetypes = type)
        with open(file, 'r') as f:
            self.df_view = pd.read_csv(f)

    def reset_callback(self):
        print('Call reset')
        self.call_back(pd.DataFrame())
        self.master.destroy()

    def ok_callback(self):
        print('Call ok')
        self.call_back(self.df_view)
        self.master.destroy()

    def cancal_callback(self):
        print('Call cancel')
        self.master.destroy()
    # <<<--- Callback Function

if __name__ == '__main__':
    def csv_excute(df):
        print(df)
    root = ThemedTk(theme='radiance')
    app = CsvGui(master=root, call_back=csv_excute)
    app.mainloop()