#!/usr/bin/env python3
# coding: utf-8

import tkinter
from tkinter import ttk
from tkinter import filedialog
from ttkthemes import ThemedTk

class CalibrationGui(ttk.Frame):
    def __init__(self, master, arm_index, num_joint_per_arm, call_back):
        super().__init__(master)
        self.arm_index = arm_index
        self.num_joint = num_joint_per_arm
        self.call_back = call_back
        # prepare frame
        master.title('Calibration' + str(arm_index))
        self.create_widgets()
        self.pack()

    def create_widgets(self):
        # frame
        button_frame =  ttk.Frame(self)
        # create button_frame
        for i in range(self.num_joint):
            calibration_button = ttk.Button(button_frame, text='Joint' + str(i), command=self.button_callback(i), width=7)
            calibration_button.grid(row = 0, column = i)
        calibration_button = ttk.Button(button_frame, text='All Joint', command=self.button_callback(self.num_joint), width=7)
        calibration_button.grid(row = 0, column = self.num_joint)
        button_frame.pack()

    # Callback Function --->>>
    def button_callback(self, i):
        def callback():
            print('Calibration Joint' + str(i))
            self.call_back(self.arm_index, i)
            self.master.destroy()
        return callback
    # <<<--- Callback Function

if __name__ == '__main__':
    def callback(arm_index, joint_index):
        print('Calibration Arm' + str(arm_index) + ' Joint' + str(joint_index))
    root = ThemedTk(theme='radiance')
    app = CalibrationGui(master=root, arm_index=0, num_joint_per_arm=3, call_back=callback)
    app.mainloop()