#!/usr/bin/env python3
# coding: utf-8

from robot_const import *
from ctypes import *

class CsvFormat(Structure):
  _pack_ = 1
  _fields_ = [
    ('right', c_float * NUM_POSE),
    ('left', c_float * NUM_POSE)
  ]