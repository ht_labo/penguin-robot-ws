#!/usr/bin/env python3
# coding: utf-8

from gain_config import *
from reference_config import *

class ControlConfig(Structure):
    _pack_ = 1
    _fields_ = [
        ('control_mode', c_uint8), 
        ('reference', Reference),
        ('gain', GainConfig),
        ('enable', c_uint8 * NUM_JOINT)
    ]