#!/usr/bin/env python3
# coding: utf-8

import tkinter
from tkinter import ttk
from tkinter import messagebox
from ttkthemes import ThemedTk
from control_config import *
from msg_data import *
from packet_data import *

from reference_gui import *
from gain_gui import *
from calibration_gui import *
from csv_gui import *

import time

from multiprocessing import Process

class MainteGuiMain:
    def __init__(self):
        print("MainteGui Constructor")

    def start(self, input_queue, output_queue):
        self.input_queue = input_queue
        self.output_queue = output_queue
        self.root = ThemedTk(theme='radiance')
        app = MainteGui(master = self.root, num_arm = NUM_ARM, num_joint_per_arm = NUM_JOINT, input_queue = self.input_queue, output_queue = self.output_queue)
        self.root.protocol("WM_DELETE_WINDOW", self.quit)
        self.root.mainloop()

    def quit(self):
        print("Call destroy")
        msg = MsgCmd(MsgType.MSG_ALL_TERMINATE)
        self.output_queue.put(msg)
        self.root.destroy()

class MainteGui(ttk.Frame):
    def __init__(self, master, num_arm, num_joint_per_arm, input_queue, output_queue):
        super().__init__(master)
        # init parameter
        self.master = master
        self.num_arm = num_arm
        self.num_joint = num_joint_per_arm
        self.input_queue = input_queue
        self.output_queue = output_queue
        self.control_config = [ControlConfig() for i in range(self.num_arm)]
        for i in range(len(self.control_config)):
            self.control_config[i].reference = open_reference("default_reference.yaml")
            self.control_config[i].gain = open_gain("default_gain.yaml")

        self.control_state = [False for i in range(self.num_arm)]
        # init frame parameter
        self.mode = tkinter.StringVar()
        self.enable_states = [[tkinter.BooleanVar(value = True) for j in range(self.num_joint)] for i in range(self.num_arm)]
        self.reference_frame = [None for i in range(self.num_arm)]
        self.gain_frame = [None for i in range(self.num_arm)]
        self.calibration_frame = [None for i in range(self.num_arm)]
        self.operation_time = tkinter.StringVar()
        self.operation_time.set(0)
        self.csv_frame = None
        style = ttk.Style()
        style.configure("control_on.TButton", foreground="red")
        style.configure("control_off.TButton", foreground="blue")
        # prepare frame
        self.master.title('MainteServer')
        self.create_widgets()
        self.pack()

        # start receive msg thread
        self.master.after(100, self.receive)

    def create_widgets(self):
        # frame
        menu_frame =  ttk.Frame(self)
        main_frame = ttk.Frame(self)
        share_frame = ttk.Frame(self)
        # create menu widgets
        label = ttk.Label(menu_frame, text = 'Control Mode')
        label.grid(row = 0, column = 0)
        mode_labels = [mode.name for mode in ControlMode]
        mode_combo = ttk.Combobox(menu_frame, textvariable = self.mode, value = mode_labels, state = "readonly", width = 6)
        mode_combo.current(0)
        mode_combo.grid(row = 0, column = 1)
        menu_frame.pack(anchor = tkinter.E)
        # create main widgets
        for i in range(self.num_arm):
            # create arm widgets
            arm_frame = ttk.Frame(main_frame)
            label = ttk.Label(arm_frame, text = 'Arm (' + str(i) + ')')
            label.grid(row = 0, column = 0)
            # control
            control_button = ttk.Button(arm_frame, text = 'Control', width = 7, style="control_off.TButton")
            control_button.configure(command = self.control_callback(i, control_button))
            control_button.grid(row = 1, column = 0)
            # enable
            for j in range(self.num_joint):
                enable_button = ttk.Checkbutton(arm_frame, text = 'Joint(' + str(j) + ')', variable = self.enable_states[i][j])
                enable_button.grid(row = 1, column = j + 1)
            # reference
            reference_button = ttk.Button(arm_frame, text = 'R', command = self.reference_callback(i), width = 1)
            reference_button.grid(row = 1, column = self.num_joint + 1)
            # gain
            gain_button = ttk.Button(arm_frame, text = 'G', command = self.gain_callback(i), width = 1)
            gain_button.grid(row = 1, column = self.num_joint + 2)
            arm_frame.pack()
            # calibration
            calibration_button = ttk.Button(arm_frame, text = 'C', command = self.calibration_callback(i), width = 1)
            calibration_button.grid(row = 1, column = self.num_joint + 3)
            arm_frame.pack()
            # create separator
            separator = ttk.Separator(main_frame)
            separator.pack(fill="both")
        main_frame.pack()
        # create share widgets
        label = ttk.Label(share_frame, text = 'Share')
        label.grid(row = 0, column = 0)
        # control
        control_button = ttk.Button(share_frame, text = 'Control', width = 7)
        control_button.configure(command = self.all_control_callback())
        control_button.grid(row = 1, column = 0)
        # operation
        operation_time_label = ttk.Label(share_frame, text = 'Operation Time [s]')
        operation_time_label.grid(row = 1, column = 1)
        operation_time = ttk.Spinbox(share_frame, textvariable = self.operation_time, from_ = 0.0, to = 600.0, increment=1.0, width=7)
        operation_time.grid(row = 1, column = 2)
        share_frame.pack(anchor = tkinter.W)
        # calibration
        calibration_button = ttk.Button(share_frame, text = 'Calibration', width = 10)
        calibration_button.configure(command = self.all_calibration_callback())
        calibration_button.grid(row = 2, column = 0)
        # csv
        csv_button = ttk.Button(share_frame, text = 'Csv', width = 10)
        csv_button.configure(command = self.csv_callback())
        csv_button.grid(row = 2, column = 1)

    # Callback Function --->>>
    def control_callback(self, i, button):
        def callback():
            self.control_config[i].control_mode = ControlMode[self.mode.get()]
            for j in range(self.num_joint):
                if self.enable_states[i][j].get() == True:
                    self.control_config[i].enable[j] = 1
                else:
                    self.control_config[i].enable[j] = 0
            if self.control_state[i]:
                print('Call control off ' + str(i) + ' Mode = ' + self.mode.get())
                self.control_state[i] =  False
                button.configure(style="control_off.TButton")
                msg = MsgCmdControl(MsgType.MSG_GUI_TO_SERVER_CONTROL_OFF, i, self.control_config[i])
                self.output_queue.put(msg)
            else:
                print('Call control on ' + str(i) + ' Mode = ' + self.mode.get())
                self.control_state[i] =  True
                button.configure(style="control_on.TButton")
                msg = MsgCmdControl(MsgType.MSG_GUI_TO_SERVER_CONTROL_ON, i, self.control_config[i])
                self.output_queue.put(msg)
        return callback

    def reference_callback(self, i):
        def callback():
            print('Call reference ' + str(i))
            if self.reference_frame[i] == None or not self.reference_frame[i].winfo_exists():
                self.reference_frame[i] = tkinter.Toplevel(self)
                ReferenceGui(self.reference_frame[i], self.mode.get(), i, self.num_joint, self.control_config[i].reference)
        return callback

    def gain_callback(self, i):
        def callback():
            print('Call gain ' + str(i))
            if self.gain_frame[i] == None or not self.gain_frame[i].winfo_exists():
                self.gain_frame[i] = tkinter.Toplevel(self)
                GainGui(self.gain_frame[i], i, self.control_config[i].gain)
        return callback

    def calibration_callback(self, i):
        def callback():
            print('Call calibration ' + str(i))
            if self.calibration_frame[i] == None or not self.calibration_frame[i].winfo_exists():
                self.calibration_frame[i] = tkinter.Toplevel(self)
                CalibrationGui(self.calibration_frame[i], i, NUM_JOINT, self.calibration_excute)
        return callback

    def all_control_callback(self):
        def callback():
            print('Call all controll')
            for i in range(self.num_arm):
                self.control_config[i].control_mode = ControlMode[self.mode.get()]
                for j in range(self.num_joint):
                    if self.enable_states[i][j].get() == True:
                        self.control_config[i].enable[j] = 1
                    else:
                        self.control_config[i].enable[j] = 0
            msg = MsgCmdAllControl(MsgType.MSG_GUI_TO_SERVER_ALL_CONTROL_ON, int(self.operation_time.get()))
            for i in range(len(self.control_config)):
                msg.config[i] = self.control_config[i]
            self.output_queue.put(msg)
        return callback

    def all_calibration_callback(self):
        def callback():
            print('Call all calibration')
            for i in range(self.num_arm):
                self.calibration_excute(i, self.num_joint)
        return callback

    def csv_callback(self):
        def callback():
            print('Call csv')
            if self.csv_frame == None or not self.csv_frame.winfo_exists():
                self.csv_frame = tkinter.Toplevel(self)
                CsvGui(self.csv_frame, self.csv_excute)
        return callback
    # <<<--- Callback Function

    # Transmit Function --->>>
    def calibration_excute(self, i, j):
        print('Calibration Arm' + str(i) + ' Joint' + str(j))
        msg = MsgCmdCalibration(i, j)
        self.output_queue.put(msg)

    def csv_excute(self, df):
        print('Csv Reset') 
        msg = MsgCmd(MsgType.MSG_GUI_TO_SERVER_CSV_RESET)
        self.output_queue.put(msg)
        print('Csv Excute ' + str(len(df)))
        for index, data in df.iterrows():
            csv_format = CsvFormat()
            # create packet
            for i in range(self.num_joint):
                # print(data)
                csv_format.right[i] = data.iloc[i + 1]
                csv_format.left[i] = data.iloc[i + self.num_joint + 1]
            inv_index = len(df) - 1 - index
            msg = MsgCmdCsvAppendReq(inv_index, csv_format)
            self.output_queue.put(msg)
    # <<<--- Transmit Function

    def receive(self):
        while not self.input_queue.empty():
            msg = self.input_queue.get()
            type = msg.header.type
            if type == MsgType.MSG_SERVER_TO_GU_CSV_APPEND_RES:
                messagebox.showinfo('Csv Mode', 'Transfer completed !!!')
                print('Receive')
        self.master.after(100, self.receive)
            

if __name__ == '__main__':
    from multiprocessing import Queue
    mq_gui_to_server = Queue()
    mq_server_to_gui = Queue()
    root = ThemedTk(theme='radiance')
    app = MainteGui(master = root, num_arm = 2, num_joint_per_arm = 3, input_queue = mq_server_to_gui, output_queue = mq_gui_to_server)
    app.mainloop()