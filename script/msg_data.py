#!/usr/bin/env python3
# coding: utf-8

from ctypes import *
from control_config import *
from csv_format import *

class MsgType(IntEnum):
    MSG_UNKNOWN = 0
    # gui to server
    MSG_GUI_TO_SERVER_CONTROL_ON = auto()
    MSG_GUI_TO_SERVER_CONTROL_OFF = auto()
    MSG_GUI_TO_SERVER_CALIBRATION = auto()
    MSG_GUI_TO_SERVER_ALL_CONTROL_ON = auto()
    MSG_GUI_TO_SERVER_ALL_CONTROL_OFF = auto()
    MSG_GUI_TO_SERVER_CSV_APPEND_REQ = auto()
    MSG_GUI_TO_SERVER_CSV_RESET = auto()
    # server to gui
    MSG_SERVER_TO_GUI_ROBOT_INFO = auto()
    MSG_SERVER_TO_GU_CSV_APPEND_RES = auto()
    # terminate
    MSG_ALL_TERMINATE = auto()

class MsgHeader(Structure):
    _pack_ = 1
    _fields_ = [
        ('size', c_uint32),
        ('type', c_uint8),
    ]
    def __init__(self, size = 0, type = 0):
        self.size = size
        self.type = type

class MsgCmd(Structure):
    _pack_ = 1
    _fields_ = [
        ('header', MsgHeader),
    ]
    def __init__(self, type = 0):
        self.header = MsgHeader(sizeof(MsgCmd), type)

class MsgCmdControl(Structure):
    _pack_ = 1
    _fields_ = [
        ('header', MsgHeader),
        ('arm_id', c_uint8),
        ('config', ControlConfig)
    ]
    def __init__(self, type = 0, arm_id = 0, config = ControlConfig()):
        self.header = MsgHeader(sizeof(MsgCmdControl), type)
        self.arm_id = arm_id
        self.config = config

class MsgCmdCalibration(Structure):
    _pack_ = 1
    _fields_ = [
        ('header', MsgHeader),
        ('arm_id', c_uint8),
        ('joint_index', c_uint8)
    ]
    def __init__(self, arm_id = 0, joint_index = 0):
        self.header = MsgHeader(sizeof(MsgCmdControl), MsgType.MSG_GUI_TO_SERVER_CALIBRATION)
        self.arm_id = arm_id
        self.joint_index = joint_index

class MsgCmdAllControl(Structure):
    _pack_ = 1
    _fields_ = [
        ('header', MsgHeader),
        ('operation_time', c_uint32),
        ('config', ControlConfig * NUM_ARM)
    ]
    def __init__(self, type, time):
        self.header = MsgHeader(sizeof(MsgCmdControl), type)
        self.operation_time = time

class MsgCmdCsvAppendReq(Structure):
    _pack_ = 1
    _fields_ = [
        ('header', MsgHeader),
        ('index', c_uint32),
        ('data', CsvFormat)
    ]
    def __init__(self, index, data):
        self.header = MsgHeader(sizeof(MsgCmdCsvAppendReq), MsgType.MSG_GUI_TO_SERVER_CSV_APPEND_REQ)
        self.index = index
        self.data = data

class MsgCmdCsvAppendRes(Structure):
    _pack_ = 1
    _fields_ = [
        ('header', MsgHeader),
        ('result', c_bool)
    ]
    def __init__(self, result = 0):
        self.header = MsgHeader(sizeof(MsgCmdCsvAppendReq), MsgType.MSG_SERVER_TO_GU_CSV_APPEND_RES)
        self.result = result

def GetMsgType(data):
    msg_header = MsgHeader()
    memmove(addressof(msg_header), data, sizeof(msg_header))
    return msg_header.type