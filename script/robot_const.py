#!/usr/bin/env python3
# coding: utf-8

from enum import *

NUM_ARM = 2
NUM_JOINT = 3
NUM_POSE = 3
XYZ = 3
RPY = 3

class ControlMode(IntEnum):
  FK = 0
  IK = 1
  EFFORT = 2
  ACT_EFFORT = 3
  OPEN_LOOP = 4
  MS = 5
  CSV = 6