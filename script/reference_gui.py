#!/usr/bin/env python3
# coding: utf-8

import tkinter
from tkinter import ttk
from tkinter import filedialog
from ttkthemes import ThemedTk
from reference_config import *

class ReferenceGui(ttk.Frame):
    def __init__(self, master, mode, arm_index, num_joint_per_arm, reference):
        super().__init__(master)
        # init parameter
        self.mode = mode
        self.num_joint = num_joint_per_arm
        self.reference = reference
        # init frame parameter
        labels = ['Joint' + str(i) for i in range(self.num_joint)]
        if self.mode == 'IK':
            labels = ['Flap', 'Feather', 'Lead-lag']
        elif self.mode == 'MS' or self.mode == 'CSV':
            self.master.destroy()
            return
        self.reference_list = [{'label' : label,
                                'type' : tkinter.StringVar(),
                                'amp' : tkinter.StringVar(),
                                'base' : tkinter.StringVar(),
                                'freq' : tkinter.StringVar(),
                                'phase' : tkinter.StringVar()}  for label in labels]
        self.reference_to_gui(self.reference)

        # prepare frame
        master.title('Reference' + str(arm_index))
        self.create_widgets()
        self.pack()

    def create_widgets(self):
        # frame
        option_frame = ttk.Frame(self)
        reference_frame = ttk.Frame(self)
        menu_frame =  ttk.Frame(self)
        # create option_frame widgets
        mode_label = ttk.Label(option_frame, text = 'Control Mode : ' + self.mode)
        mode_label.grid(row = 0, column = 0)
        open_button = ttk.Button(option_frame, text='Open', command=self.open_callback, width=7)
        open_button.grid(row = 0, column = 1)
        save_button = ttk.Button(option_frame, text='Save', command=self.save_callback, width=7)
        save_button.grid(row = 0, column = 2)
        #set option_frame widgets
        option_frame.pack(anchor = tkinter.E)
        # create reference_frame widgets
        wave_form_labels = ['Type', 'Amp', 'Base', 'Freq', 'Phase']
        for i in range(len(wave_form_labels)):
            label = ttk.Label(reference_frame, text=wave_form_labels[i])
            label.grid(row = i + 1, column = 0)
        wave_type_labels = [type.name for type in WaveType]
        for i in range(len(self.reference_list)):
            label = ttk.Label(reference_frame, text=self.reference_list[i]['label'])
            wave_type_combo = ttk.Combobox(reference_frame, textvariable=self.reference_list[i]['type'], value=wave_type_labels, state="readonly", width=7)
            wave_type_combo.set(self.reference_list[i]['type'].get())
            amp_spin = ttk.Spinbox(reference_frame, textvariable=self.reference_list[i]['amp'], from_=0.0,to=180.0,increment=0.1, width=7)
            base_spin = ttk.Spinbox(reference_frame, textvariable=self.reference_list[i]['base'], from_=-180.0,to=180.0,increment=0.1, width=7)
            freq_spin = ttk.Spinbox(reference_frame, textvariable=self.reference_list[i]['freq'], from_=0.0,to=20.0,increment=0.1, width=7)
            phase_spin = ttk.Spinbox(reference_frame, textvariable=self.reference_list[i]['phase'], from_=-180.0,to=180.0,increment=0.1, width=7)
            label.grid(row = 0, column = i + 1)
            wave_type_combo.grid(row = 1, column = i + 1)
            amp_spin.grid(row = 2, column = i + 1)
            base_spin.grid(row = 3, column = i + 1)
            freq_spin.grid(row = 4, column = i + 1)
            phase_spin.grid(row = 5, column = i + 1)
        # set reference_frame widgets
        reference_frame.pack()
        # create menu_frame widgets
        cancel_button = ttk.Button(menu_frame, text='Cancel', command=self.cancal_callback, width=7)
        ok_button = ttk.Button(menu_frame, text='OK', command=self.ok_callback, width=7)
        cancel_button.grid(row = 0, column = 0)
        ok_button.grid(row = 0, column = 1)
        #set menu_frame widgets
        menu_frame.pack(anchor = tkinter.E)

    def reference_to_gui(self, reference):
        reference_list = []
        if self.mode == 'FK':
            reference_list = reference.fk
        elif self.mode == 'IK':
            reference_list = reference.ik
        elif self.mode == 'EFFORT':
            reference_list = reference.effort
        elif self.mode == 'ACT_EFFORT':
            reference_list = reference.act_effort
        elif self.mode == 'OPEN_LOOP':
            reference_list = reference.open_loop
        for i in range(len(self.reference_list)):
            self.reference_list[i]['type'].set(WaveType(reference_list[i].type).name)
            self.reference_list[i]['amp'].set(reference_list[i].amp)
            self.reference_list[i]['base'].set(reference_list[i].base)
            self.reference_list[i]['freq'].set(reference_list[i].freq)
            self.reference_list[i]['phase'].set(reference_list[i].phase)

    def gui_to_reference(self):
        ret = self.reference
        for i in range(len(self.reference_list)):
            wave_form = WaveForm(WaveType[self.reference_list[i]['type'].get()],
                                 float(self.reference_list[i]['amp'].get()),
                                 float(self.reference_list[i]['base'].get()),
                                 float(self.reference_list[i]['freq'].get()),
                                 float(self.reference_list[i]['phase'].get()))
            if self.mode == 'FK':
                ret.fk[i] = wave_form
            elif self.mode == 'IK':
                ret.ik[i] = wave_form
            elif self.mode == 'EFFORT':
                ret.effort[i] = wave_form
            elif self.mode == 'ACT_EFFORT':
                ret.act_effort[i] = wave_form
            elif self.mode == 'OPEN_LOOP':
                ret.open_loop[i] = wave_form
        return ret

    # Callback Function --->>>
    def open_callback(self):
        print('call open')
        type = [('Refernce File','*.yml')] 
        reference_file = filedialog.askopenfilename(filetypes = type)
        self.reference_to_gui(open_reference(reference_file))

    def save_callback(self):
        print('call save')
        type = [('Refernce File','*.yml')] 
        reference_file = filedialog.asksaveasfilename(filetypes = type) 
        save_reference(reference_file, self.gui_to_reference())

    def ok_callback(self):
        print('Call ok')
        self.reference = self.gui_to_reference()
        self.master.destroy()

    def cancal_callback(self):
        print('Call cancel')
        self.master.destroy()
    # <<<--- Callback Function

if __name__ == '__main__':
    reference = Reference()
    root = ThemedTk(theme='radiance')
    app = ReferenceGui(master=root, mode='FK', arm_index=0, num_joint_per_arm = 3, reference = reference)
    app.mainloop()