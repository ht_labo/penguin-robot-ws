#!/usr/bin/env python3
# coding: utf-8

from mainte_server_main import *
from mainte_gui_main import *
from multiprocessing import Queue

def main():
    mq_gui_to_server = Queue()
    mq_server_to_gui = Queue()

    # server process
    mainte_server_thread = MainteServerMain(mq_gui_to_server, mq_server_to_gui)
    mainte_server_thread.start()

    # gui process (main process)
    mainte_gui = MainteGuiMain()
    mainte_gui.start(mq_server_to_gui, mq_gui_to_server)

    mainte_server_thread.join()

if __name__ == '__main__':
    main()