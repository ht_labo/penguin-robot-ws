# penguin-robot-ws
This repository is a tool for developing Penguin Robot
## Requirement
### Hardware
[STM32F767ZIT6](https://os.mbed.com/platforms/ST-Nucleo-F767ZI/)

[RI50_KV100](https://store.tmotor.com/goods.php?id=856)
### Software
PlatformIO IDE V2.5.0
+ ST STM32 15.4.0
## Install
``` bash
git clone git@gitlab.com:ht_labo/penguin-robot-ws.git # clone
cd penguin-robot-ws
git submodule update --init --recursive # update submodule
```
## Usage
### Change Control Paramenter
Edit src/control_data/control_config.h
```c
void GetControlConfig(ControlConfig& config)
```
### Change Hardware Paramter
Edit src/controller/hardware_config.h
```c
void GetHardwareConfig(HardwareConfig& config)
```

### Maintenance GUI
```sh
cd script
python3 main.py
```

## Get Log
### Plotjuggler
https://github.com/facontidavide/PlotJuggler#installation

| parameter | value |
| ---- | ---- |
| Streaming | UDP Server |
| Port | 50000 |
| Message Protocol | msgpack |